#include "jit.h"
#include "debug.h"

#include "llvm/IR/IntrinsicInst.h"

#include <sstream>

using namespace llvm;

static BasicBlock* DLJIT_contiguous_push(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbInner, bool pack) {
    IRBuilder<> *builder = env->builder();
    const Dataloop *dl = env->dl();

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(dl[sp].count);

    builder->SetInsertPoint(bb);
    Value *base = nullptr;
    if(pack)
        base = builder->CreateLoad(env->inbase());
    else 
        base = builder->CreateLoad(env->outbase());

    {
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    builder->CreateStore(base, baseGEP);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmCount, leftGEP);
    }

    builder->CreateBr(bbInner);

    return bb;
}

static BasicBlock* DLJIT_contiguous_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    const Dataloop *dl = env->dl();
    Function *fn = env->fn();
    std::stringstream ss;

    Value *stack = env->stack();

    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmBaseExtent = env->int64Const(dl[sp].s.c_t.baseextent);

    ss.str(std::string());
    ss << "cont" << sp << "_done";
    BasicBlock *bbDone = BasicBlock::Create(*context, ss.str(), fn, bbIns);
    ss.str(std::string());
    ss << "cont" << sp << "_notdone";
    BasicBlock *bbNotDone = BasicBlock::Create(*context, ss.str(), fn, bbIns);
    
    /* Check if the type is done */
    builder->SetInsertPoint(bb);
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    // if(--stack[sp].countLeft == 0)
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX); 
    Value *left = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(left, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbNotDone);

    /* Not done with the type */
    builder->SetInsertPoint(bbNotDone);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *base = builder->CreateLoad(baseGEP);
    
    Value *idx = builder->CreateSub(llvmCount, newLeft);
    Value *offset = builder->CreateMul(idx, llvmBaseExtent);
    Value *newBase = builder->CreateGEP(base, offset);
    if(pack) 
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());

    builder->CreateBr(bbInner);

    /* Finished with the type */
    builder->SetInsertPoint(bbDone);
    builder->CreateBr(bbOuter);

    return bb;
}

BasicBlock* DLJIT_contiguous(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    std::stringstream ss;

    ss.str(std::string());
    ss << "contig" << sp << "_pop";
    BasicBlock *bbPop = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) return nullptr;

    ss.str(std::string());
    ss << "contig" << sp << "_push";
    BasicBlock *bbPush = BasicBlock::Create(*context, ss.str(), fn, bbInner);

    auto rvPop = DLJIT_contiguous_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_contiguous_push(env, sp, bbPush, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;

}

BasicBlock* DLJIT_contigfinal(BuildEnv* env, int sp, BasicBlock* bbOuter, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();

    long basesize = dl[sp].s.c_t.basesize;

    Value *stack = env->stack();
    Value *llvmCount = env->int64Const(dl[sp].count);
    Value *llvmBasesize = env->int64Const(basesize);

    auto bb = BasicBlock::Create(*context, "cf", fn, bbIns);
    auto bbCheck = BasicBlock::Create(*context, "check", fn, bbIns);
    auto bbNotEnough = BasicBlock::Create(*context, "notenough", fn, bbIns);
    auto bbCheckCopy = BasicBlock::Create(*context, "ck_copy", fn, bbIns);
    auto bbCopyLong = BasicBlock::Create(*context, "cpy_long", fn, bbIns);
    auto bbCopyShort = BasicBlock::Create(*context, "cpy_short", fn, bbIns);
    auto bbCopyDone = BasicBlock::Create(*context, "cpy_done", fn, bbIns);
    auto bbPreparePop = BasicBlock::Create(*context, "beforepop", fn, bbIns);
    auto bbSave = BasicBlock::Create(*context, "save", fn, bbIns);
    
	std::vector<Type*> tys 
        = { env->int8PtrTy(), env->int8PtrTy(), env->int64Ty() };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), tys, false);
    auto memcpyFn =cast<Function>(module->getOrInsertFunction("memcpy",fnType));
    Function *saveStateFn = DLJIT_gen_save_state(env);

    /* Entry */
    builder->SetInsertPoint(bb);
    auto completed=builder->CreateAlloca(env->int64Ty(), nullptr, "completed");

    Value *inbase = builder->CreateLoad(env->inbase());
    Value *outbase = builder->CreateLoad(env->outbase());
    Value *sizeLeft = builder->CreateLoad(env->sizeLeft());

    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    if(pack) {
        builder->CreateStore(inbase, baseGEP);
    } else {
        builder->CreateStore(outbase, baseGEP);
    }

    Value *oldLeft = builder->CreateLoad(leftGEP);
    Value *isLeft0 = builder->CreateICmpEQ(oldLeft, env->int64Const(0));
    Value *left = builder->CreateSelect(isLeft0, llvmCount, oldLeft);
    builder->CreateStore(left, leftGEP);
    builder->CreateStore(left, completed);

    builder->CreateCondBr(builder->CreateICmpEQ(sizeLeft, env->int64Const(0)),
                          bbSave, bbCheck);

    /* Check if there's enough to copy the rest */
    builder->SetInsertPoint(bbCheck);
    Value *partial = builder->CreateLoad(env->partial());
    Value *remaining = builder->CreateSub(builder->CreateMul(left,llvmBasesize),
                                          builder->CreateURem(partial,llvmBasesize));
    builder->CreateStore(left, completed);
    Value *cmp = builder->CreateICmpUGE(remaining, sizeLeft);
    Value *tocopy = builder->CreateSelect(cmp, sizeLeft, remaining);
    builder->CreateCondBr(cmp, bbNotEnough, bbCheckCopy);

    /* Not enough */
    {
    builder->SetInsertPoint(bbNotEnough);
    Value *comp = builder->CreateUDiv(tocopy, llvmBasesize);
    builder->CreateStore(comp, completed);
    builder->CreateStore(builder->CreateURem(tocopy, llvmBasesize), 
                         env->partial());
    builder->CreateBr(bbCheckCopy);
    }

    /* Copy */
    builder->SetInsertPoint(bbCheckCopy);
    builder->CreateCondBr(builder->CreateICmpUGT(tocopy, env->int64Const(DLJIT_THRESHOLD_USE_MEMCPY)),
                          bbCopyLong, bbCopyShort);

    builder->SetInsertPoint(bbCopyLong);
    builder->CreateCall3(memcpyFn, outbase, inbase, tocopy);
    builder->CreateBr(bbCopyDone);

    builder->SetInsertPoint(bbCopyShort);
    DLJIT_gen_memcpy_short(env, outbase, inbase, tocopy, 1, fn, bbCopyShort, bbCopyDone);

    builder->SetInsertPoint(bbCopyDone);
    Value *newleft = builder->CreateSub(left, builder->CreateLoad(completed));
    builder->CreateStore(newleft, leftGEP);
    builder->CreateStore(builder->CreateGEP(inbase, tocopy), env->inbase());
    builder->CreateStore(builder->CreateGEP(outbase, tocopy), env->outbase());
    builder->CreateStore(builder->CreateSub(sizeLeft, tocopy), env->sizeLeft());
    builder->CreateCondBr(builder->CreateICmpUGT(remaining, tocopy),
                          bbSave, bbPreparePop);


    /* Prepare to pop */
    builder->SetInsertPoint(bbPreparePop);
    builder->CreateStore(env->int64Const(0), env->partial());
    builder->CreateBr(bbOuter);

    /* Save state */
    builder->SetInsertPoint(bbSave);
    Value *newBase = nullptr;
    if(pack) {
        newBase = builder->CreateLoad(env->inbase());
    } else { 
        newBase = builder->CreateLoad(env->outbase());
    }
    builder->CreateStore(newBase, baseGEP);

    unsigned idx = env->addResumeBlock(bb);
    Value *llvmSp = env->int64Const(sp);
    Value *llvmIdx = env->int64Const(idx);
    builder->CreateCall4(saveStateFn, 
                         llvmSp, 
                         llvmIdx, 
                         builder->CreateLoad(env->partial()), 
                         getFnArg(fn,PARAM_STATE));
    builder->CreateStore(getFnArg(fn, PARAM_SIZE), getFnArg(fn, PARAM_COPIED));
    builder->CreateRet(ConstantInt::get(env->int32Ty(), 1));

    return bb;
}

