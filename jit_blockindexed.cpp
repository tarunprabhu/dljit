#include "jit.h"
#include "debug.h"
#include <sstream>

using namespace llvm;

static BasicBlock* DLJIT_blkidx_push(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbInner, bool pack) {
    IRBuilder<> *builder = env->builder();
    const Dataloop *dl = env->dl();

    long offset0 = dl[sp].s.bi_t.offsets[0];

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmBlklen = env->int64Const(dl[sp].s.bi_t.blklen);
    
    builder->SetInsertPoint(bb);
    Value *base = nullptr;
    if(pack) 
        base = builder->CreateLoad(env->inbase());
    else
        base = builder->CreateLoad(env->outbase());

    {
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    builder->CreateStore(base, baseGEP);      // stack[sp].base = base
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmCount, leftGEP); // stack[sp].left = dl[sp].count 
    }

    if(dl[sp].kind != DL_BLOCKINDEX1) {
        Value *selm = builder->CreateConstGEP2_64(stack, 0, sp+1);
        Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
        builder->CreateStore(base, baseGEP);       // stack[sp+1].base = base
        Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
        builder->CreateStore(llvmBlklen, leftGEP); // stack[sp+1].left = blklen
    }
    
    // inbase/outbase += dl[sp].s.bi_t.offsets[0]
    Value *newBase = builder->CreateConstGEP1_64(base, offset0);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else
        builder->CreateStore(newBase, env->outbase());

    builder->CreateBr(bbInner);

    return bb;
}

static BasicBlock* DLJIT_blkidx_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();
    std::stringstream ss;

    ss.str(std::string());
    ss << "blkidx" << sp << "_done";
    BasicBlock *bbDone = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    ss.str(std::string());
    ss << "blkidx" << sp << "_left";
    BasicBlock *bbLeft = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmBlklen = env->int64Const(dl[sp].s.bi_t.blklen);
    Constant *llvmOffsetPtr = env->int64Const((long)dl[sp].s.bi_t.offsets);

    // if(stack[sp].countLeft == 0) goto bbOuter;
    builder->SetInsertPoint(bb);
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX); 
    Value *left = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(left, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbLeft);

    // Not done
    builder->SetInsertPoint(bbLeft);
    Value *idx = builder->CreateSub(llvmCount, newLeft);

    // inbase = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
    Value *offsetPtr = builder->CreateIntToPtr(llvmOffsetPtr,env->int64PtrTy());
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *offsetGEP = builder->CreateGEP(offsetPtr, idx);
    Value *newBase = builder->CreateGEP(builder->CreateLoad(baseGEP),
                                        builder->CreateLoad(offsetGEP));
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());

    if(dl[sp].kind != DL_BLOCKINDEX1) {
        // stack[sp+1].countLeft = dl[sp].s.bi_t.blklen
        Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
        Value *left1GEP = builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
        builder->CreateStore(llvmBlklen, left1GEP);
    }
    builder->CreateBr(bbInner);

    // goto bbOuter
    builder->SetInsertPoint(bbDone);
    builder->CreateBr(bbOuter);

    return bb;
}

BasicBlock* DLJIT_blockindexed(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    std::stringstream ss;

    ss.str(std::string());
    ss << "blkidx" << sp << "_pop";
    BasicBlock *bbPop = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) return nullptr;

    ss.str(std::string());
    ss << "blkidx" << sp << "_push";
    BasicBlock *bbPush = BasicBlock::Create(*context, ss.str(), fn, bbInner);

    auto rvPop = DLJIT_blkidx_pop(env, sp, bbPop, bbOuter, bbInner, bbIns,pack);
    auto rvPush = DLJIT_blkidx_push(env, sp, bbPush, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

static BasicBlock* DLJIT_blkfinal_push(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();
    
    long size = dl[sp].size;
    long count = dl[sp].count;
    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    long blklen = dl[sp].s.bi_t.blklen;
    long oldsize = dl[sp].s.bi_t.oldsize;
    void* offsets = (void*)dl[sp].s.bi_t.offsets;
    long blksize = blklen * oldsize;

    Value *stack = env->stack();
    Constant *llvmSize = env->int64Const(size);
    Constant *llvmCount = env->int64Const(count);
    Constant *llvmOffsets = env->int64Const((long)offsets);
    Constant *llvmBlklen = env->int64Const(blklen);
    Constant *llvmBlksize = env->int64Const(blksize);

    Function *fnBlkFinalAll = DLJIT_gen_blockindexedfinal(env, count, blklen, offsets, oldsize, extent, flags, pack);
    Function *fnBlkFinalSome = DLJIT_gen_blockindexedfinal(env, blklen, offsets, oldsize, extent, flags, pack);

    auto bbAll = BasicBlock::Create(*context, "bf_push_all", fn, bbIns);
    auto bbCount = BasicBlock::Create(*context, "bf_push_count", fn, bbIns);
    auto bbSome = BasicBlock::Create(*context, "bf_push_some", fn, bbIns);
    auto bbNone = BasicBlock::Create(*context, "bf_push_none", fn, bbIns);

    /* Entry */
    builder->SetInsertPoint(bb);
    Value *inbase = builder->CreateLoad(env->inbase());
    Value *outbase = builder->CreateLoad(env->outbase());
    Value *sizeleft = builder->CreateLoad(env->sizeLeft());
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());

    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    // stack[sp].base = (in/out)base;
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    if(pack)
        builder->CreateStore(inbase, baseGEP);
    else
        builder->CreateStore(outbase, baseGEP);
    Value *currBase = builder->CreateLoad(baseGEP);
    // stack[sp].countLeft = dl[sp].count
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmCount, leftGEP);
    Value *currLeft = builder->CreateLoad(leftGEP);

    builder->CreateCondBr(builder->CreateICmpSGE(sizeleft, llvmSize),
                          bbAll, bbCount);

    /* All */
    builder->SetInsertPoint(bbAll);
    {
    Value *newInbase = nullptr, *newOutbase = nullptr;
    if(pack) {
        newOutbase = builder->CreateCall2(fnBlkFinalAll, outbase, currBase);
        newInbase = builder->CreateConstGEP1_64(currBase, extent);
    } else {
        newInbase = builder->CreateCall2(fnBlkFinalAll, currBase, inbase);
        newOutbase = builder->CreateConstGEP1_64(currBase, extent);
    }
    builder->CreateStore(newInbase, env->inbase());
    builder->CreateStore(newOutbase, env->outbase());
    builder->CreateStore(env->int64Const(0), leftGEP);
    Value *newSizeLeft = builder->CreateSub(sizeleft, llvmSize);
    builder->CreateStore(newSizeLeft, env->sizeLeft());
    builder->CreateBr(bbOuter);
    }

    /* Count */
    builder->SetInsertPoint(bbCount);
    Value *ct = builder->CreateSDiv(sizeleft, llvmBlksize);
    builder->CreateCondBr(builder->CreateICmpSGT(ct, env->int64Const(0)),
                          bbSome, bbNone);

    /* Some */
    builder->SetInsertPoint(bbSome);
    {
    if(pack) {
        Value *newPtr = builder->CreateCall4(fnBlkFinalSome, outbase, currBase, env->int64Const(0), ct);
        builder->CreateStore(newPtr, env->outbase());
    } else {
        Value *newPtr = builder->CreateCall4(fnBlkFinalSome, currBase, inbase, env->int64Const(0), ct);
        builder->CreateStore(newPtr, env->inbase());
    }
    // sizeLeft -= count*blklen*oldsize
    Value *copied = builder->CreateMul(ct, llvmBlksize);
    builder->CreateStore(builder->CreateSub(sizeleft, copied), env->sizeLeft());
    // stack[sp].countLeft -= count
    builder->CreateStore(builder->CreateSub(currLeft, ct), leftGEP);
    builder->CreateBr(bbNone);
    }

    /* None */
    builder->SetInsertPoint(bbNone);
    {
    PHINode *idx = builder->CreatePHI(env->int64Ty(), 2);
    idx->addIncoming(env->int64Const(0), bbCount);
    idx->addIncoming(ct, bbSome);
    // stack[sp+1].countLeft = blklen*oldsize
    Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *left1GEP = builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
    builder->CreateStore(llvmBlklen, left1GEP);
    // (in/out)base = stack[sp].base + offsets[idx];
    Value *offset = builder->CreateLoad(builder->CreateGEP(offsetsPtr, idx));
    Value *newBase = builder->CreateGEP(currBase, offset);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());

    // goto dlpush
    builder->CreateBr(bbInner);
    }

    return bb;
}

static BasicBlock* DLJIT_blkfinal_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();

    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    long blklen = dl[sp].s.bi_t.blklen;
    void* offsets = (void*)dl[sp].s.bi_t.offsets;
    long oldsize = dl[sp].s.bi_t.oldsize;
    long blksize = blklen * oldsize;

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmBlklen = env->int64Const(blklen);
    Constant *llvmBlksize = env->int64Const(blksize);
    Constant *llvmOffsets = env->int64Const((long)offsets);
    
    Function *fnBlkFinalSome = DLJIT_gen_blockindexedfinal(env, blklen, offsets, oldsize, extent, flags, pack);

    auto bbCount = BasicBlock::Create(*context, "bf_pop_count", fn, bbIns);
    auto bbSome = BasicBlock::Create(*context, "bf_pop_some", fn, bbIns);
    auto bbNone = BasicBlock::Create(*context, "bf_pop_none", fn, bbIns);
    auto bbDone = BasicBlock::Create(*context, "bf_pop_done", fn, bbIns);

    /* Entry */
    builder->SetInsertPoint(bb);
    Value *count = builder->CreateAlloca(env->int64Ty(), nullptr, "count");
    Value *inbase = builder->CreateLoad(env->inbase());
    Value *outbase = builder->CreateLoad(env->outbase());
    Value *sizeleft = builder->CreateLoad(env->sizeLeft());

    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *currBase = builder->CreateLoad(baseGEP);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    Value *currLeft = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(currLeft, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbCount);

    /* count */
    builder->SetInsertPoint(bbCount);
    Value *idx = builder->CreateSub(llvmCount, newLeft);
    {
    Value *ct = builder->CreateSDiv(sizeleft, llvmBlksize);
    builder->CreateStore(ct, count);
    builder->CreateCondBr(builder->CreateICmpSGT(ct, env->int64Const(0)),
                          bbSome, bbNone);
    }

    /* if(count > 0) */
    builder->SetInsertPoint(bbSome);
    {
    Value *tmpCt = builder->CreateLoad(count);
    Value *cond = builder->CreateICmpSGT(tmpCt, newLeft);
    Value *ct = builder->CreateSelect(cond, newLeft, tmpCt);
    builder->CreateStore(ct, count);
    if(pack) {
        Value *newBase = builder->CreateCall4(fnBlkFinalSome, outbase, currBase, idx, builder->CreateAdd(idx, ct));
        builder->CreateStore(newBase, env->outbase());
    } else {
        Value *newBase = builder->CreateCall4(fnBlkFinalSome, currBase, inbase, idx, builder->CreateAdd(idx, ct));
        builder->CreateStore(newBase, env->inbase());
    }
    Value *copied = builder->CreateMul(ct, llvmBlksize);
    Value *newSizeLeft = builder->CreateSub(sizeleft, copied);
    builder->CreateStore(newSizeLeft, env->sizeLeft());
    Value *updateLeft = builder->CreateSub(newLeft, ct);
    builder->CreateStore(updateLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(updateLeft, env->int64Const(0)),
                          bbDone, bbNone);
    }

    /* None */
    builder->SetInsertPoint(bbNone);
    {
    // idx += count 
    Value *newidx = builder->CreateAdd(idx, builder->CreateLoad(count));
    // stack[sp+1].countLeft = blksize
    Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *left1GEP = builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
    builder->CreateStore(llvmBlklen, left1GEP);
    // (in/out)base = base + offsets[idx]
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());
    Value *offset = builder->CreateLoad(builder->CreateGEP(offsetsPtr, newidx));
    Value *newBase = builder->CreateGEP(currBase, offset);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    }
    builder->CreateBr(bbInner);

    /* if(stack[sp].countLeft == 0) */
    builder->SetInsertPoint(bbDone);
    {
    Value *newBase = builder->CreateConstGEP1_64(currBase, extent);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    builder->CreateBr(bbOuter);
    }

    return bb;
}

BasicBlock* DLJIT_blockindexedfinal(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();

    BasicBlock *bbPop = BasicBlock::Create(*context, "bf_pop", fn, bbIns);
    BasicBlock *bbInner = DLJIT_contigfinal(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner)
        return nullptr;
    BasicBlock *bbPush = BasicBlock::Create(*context,"bf_push", fn, bbInner);
    auto rvPop = DLJIT_blkfinal_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_blkfinal_push(env, sp, bbPush, bbOuter, bbInner, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}
