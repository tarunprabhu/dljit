DAME_TOOLCHAIN_DIR=$$HOME/research/datatypes/toolchain/install
LLVMFLAGS = -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS

CXX = g++
CXXFLAGS = -O3 -Wall -std=c++11 -fPIC $(LLVMFLAGS)
CXXLIBS = -L$$DAME_TOOLCHAIN_DIR/lib -lLLVM-3.6.0 -lrt
CXXINCLUDE = -I$$DAME_TOOLCHAIN_DIR/include -I$(DAME_TOOLCHAIN_DIR)/../../mpich/install-clean/include

TARGET=libdame-llvm.so

default: $(TARGET)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(CXXINCLUDE) -c -fPIC -o $@ $^

$(TARGET): $(patsubst %.cpp, %.o, $(filter-out testdrv.cpp, $(wildcard *.cpp)))
	$(CXX) -fPIC -shared -o $@ $^ $(CXXLIBS)

clean:
	rm -f *.o *.so
