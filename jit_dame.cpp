// Code for stuff that is specific to DAME 

#include "jit.h"

#include <sstream>

using namespace llvm;

static BasicBlock* DLJIT_contigchild_push(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();

    /* Push */
    builder->SetInsertPoint(bb);
    builder->CreateBr(bbInner);

    return bb;
}

static BasicBlock* DLJIT_contigchild_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();
    std::stringstream ss;
    
    Value *stack = env->stack();

    ss.str(std::string());
    ss << "cc" << sp << "_done";
    BasicBlock *bbDone = BasicBlock::Create(*context, ss.str(), fn, bbIns);
    
    ss.str(std::string());
    ss << "cc" << sp << "_left";
    BasicBlock *bbLeft = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    /* Pop */
    builder->SetInsertPoint(bb);
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    // if(--stack[sp].countLeft == 0)
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    Value *left = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(left, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbLeft);

    // Not done
    builder->SetInsertPoint(bbLeft);
    builder->CreateBr(bbInner);
    
    // Done
    builder->SetInsertPoint(bbDone);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *base = builder->CreateLoad(baseGEP);
    Value *newBase = builder->CreateConstGEP1_64(base, dl[sp].extent);
    if(pack) 
        builder->CreateStore(newBase, env->inbase());
    else
        builder->CreateStore(newBase, env->outbase());
    builder->CreateBr(bbOuter);

    return bb;
}

BasicBlock* DLJIT_contigchild(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    std::stringstream ss;

    ss.str(std::string());
    ss << "cc" << sp << "_pop";
    BasicBlock *bbPop = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) 
        return nullptr;

    ss.str(std::string());
    ss << "cc" << sp << "_push";
    BasicBlock *bbPush = BasicBlock::Create(*context, ss.str(), fn, bbInner);

    auto rvPop = DLJIT_contigchild_pop(env,sp,bbPop,bbOuter,bbInner,bbIns,pack);
    auto rvPush = DLJIT_contigchild_push(env, sp, bbPush, bbInner,bbInner,pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

BasicBlock* DLJIT_returnto(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    /* TODO: Implement this */
    assert(0 && "NOT IMPLEMENTED");
    if(pack) {
        return DLJIT_gen_loop(env, sp+1, bbOuter, bbIns, pack);
    } else {
        //
    }
}


