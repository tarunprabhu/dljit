#include "jit.h"
#include "debug.h"

#include "llvm/IR/Intrinsics.h"
#include "llvm/Support/raw_ostream.h"

#include <sstream>

using namespace llvm;

std::string getMemcpyName(long blklen, long oldsize, bool aligned) {
    std::stringstream ss;
    if(aligned) 
        ss << "my_memcpy_" << blklen << "_" << oldsize << "_aligned";
    else 
        ss << "my_memcpy_" << blklen << "_" << oldsize << "_unaligned";
    return ss.str();
}

Function* DLJIT_gen_save_state(BuildEnv *env) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module(); 

    std::string fname = "DLJIT_save_state";
    Function *fn = module->getFunction(fname);
    if(fn)
        return fn;

    std::vector<Type*> param_ts = { 
        env->int64Ty(),   /* Stack pointer */
        env->int64Ty(),   /* Index into the resumeArray to get blockaddress */
        env->int64Ty(),   /* Bytes already copied in partial block */
        env->statePtrTy() /* State pointer */ };
    FunctionType *fnType = FunctionType::get(env->voidTy(), param_ts, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));
    fn->addFnAttr(Attribute::AlwaysInline);

    Value *state = getFnArg(fn, param_ts.size()-1);

    auto bbExit = BasicBlock::Create(*context, "exit", fn);
    auto bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);

    /* Entry */
    builder->SetInsertPoint(bbEntry);

    Value *spGEP = builder->CreateStructGEP(state, STATE_SP_IDX);
    builder->CreateStore(getFnArg(fn, 0), spGEP);
    /* Since we are hijacking the dataloop state object without tampering 
     * with its contents too much, we have to do this nastiness of saving 
     * the integer in a pointer variable. This could cause all manner of 
     * difficulty if JITted and non-JITted code are mixed, but that is not 
     * an anticipated use case, so I'm going to leave it as is for now */
    Value *data = builder->CreateIntToPtr(getFnArg(fn, 1), env->int8PtrTy());
    Value *dlGEP = builder->CreateStructGEP(state, STATE_DL_IDX);
    builder->CreateStore(data, dlGEP);
    Value *partialGEP = builder->CreateStructGEP(state, STATE_PARTIAL_IDX);
    builder->CreateStore(getFnArg(fn,2), partialGEP);

    builder->CreateBr(bbExit);

    /* Exit */
    builder->SetInsertPoint(bbExit);
    builder->CreateRetVoid();

    return fn;
}

static uint64_t gen_copy_n(BuildEnv *env, Value *dst, Value *src, PointerType *ptrTy, uint64_t rem, uint64_t copied, bool aligned=false) {
    IRBuilder<> *builder = env->builder();
    if(rem == 0)
        return 0;

    Type *inner = ptrTy->getElementType();
    uint64_t width = inner->getPrimitiveSizeInBits()/8;
    if(width == 0) {
        uint64_t element_width = inner->getScalarSizeInBits()/8;
        width = element_width*ptrTy->getVectorNumElements();
    }
    
    if(rem < width) 
        return rem;

    auto offsetSrc = builder->CreateConstGEP1_64(src, copied);
    auto currSrc = builder->CreatePointerCast(offsetSrc, ptrTy);
    auto offsetDst = builder->CreateConstGEP1_64(dst, copied);
    auto currDst = builder->CreatePointerCast(offsetDst, ptrTy);
    for(uint64_t i = 0; rem >= width; rem -= width, i++) {
        auto finalsrc = builder->CreateConstGEP1_64(currSrc,i);
        auto tmp = builder->CreateAlignedLoad(finalsrc, 1);
        auto finaldst = builder->CreateConstGEP1_64(currDst,i);
        if(aligned)
            builder->CreateAlignedStore(tmp, finaldst, 8);
        else 
            builder->CreateAlignedStore(tmp, finaldst, 1);
    }
    return rem;
}

static Function* gen_memcpy_n(BuildEnv *env, int64_t blklen, int64_t oldsize, int64_t distance, bool aligned) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module(); 

    uint64_t blksize = blklen*oldsize;
    
    std::string fname = getMemcpyName(blklen, oldsize, aligned);
    Function *fn = module->getFunction(fname);
    if(fn)
        return fn;

    std::vector<Type*> tys = { env->int8PtrTy(), env->int8PtrTy() };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), tys, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));
    fn->addFnAttr(Attribute::AlwaysInline);

    std::vector<Type*> memcpyTys = { env->int8PtrTy(), env->int8PtrTy(), env->int64Ty() };
    // auto memcpyFnTy = FunctionType::get(env->int8PtrTy(), memcpyTys, false);
    // auto memcpy = cast<Function>(module->getOrInsertFunction("memcpy", memcpyFnTy));
    Value *memcpy = Intrinsic::getDeclaration(module, Intrinsic::memcpy, memcpyTys);

    
    Argument *argdst = getFnArg(fn,0);
    Argument *argsrc = getFnArg(fn,1);
    AttrBuilder attrBuilder;
    attrBuilder.addAlignmentAttr((aligned ? 16 : 1));
    argdst->addAttr(AttributeSet::get(*context, 0, attrBuilder));
    argsrc->addAttr(AttributeSet::get(*context, 0, attrBuilder));

    Value *dst = argdst;
    Value *src = argsrc;

    IntegerType *ity = env->int8Ty();
    switch(oldsize) {
    case 1: ity = env->int8Ty(); break;
    case 2: ity = env->int16Ty(); break;
    case 4: ity = env->int32Ty(); break;
    case 8: ity = env->int64Ty(); break;
    default: break;
    }

    auto v128Ptr = env->vecPtrTy(env->int8Ty(), DLJIT_VECWIDTH_2/1);
    auto i64Ptr = env->int64PtrTy();
    auto i32Ptr = env->int32PtrTy();
    auto i16Ptr = env->int16PtrTy();
    auto i8Ptr = env->int8PtrTy();
        
    auto bbExit = BasicBlock::Create(*context, "exit", fn);
    auto bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);

    builder->SetInsertPoint(bbEntry);
    if(blksize >= DLJIT_THRESHOLD_USE_MEMCPY) {
        // builder->CreateCall3(memcpy, dst, src, env->int64Const(blksize));
        if(aligned)
            builder->CreateCall5(memcpy, dst, src, env->int64Const(blksize), env->int32Const(16), env->int1Const(true));
        else
            builder->CreateCall5(memcpy, dst, src, env->int64Const(blksize), env->int32Const(0), env->int1Const(true));
    } else if(blksize <= DLJIT_THRESHOLD_FULL_UNROLL) {
        /* Available instruction widths */
        uint64_t rem = blksize;
        rem = gen_copy_n(env, dst, src, v128Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i64Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i32Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i16Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i8Ptr, rem, blksize-rem, aligned);
    } else {
        auto bbHead = BasicBlock::Create(*context, "head", fn, bbExit);
        auto bbBody = BasicBlock::Create(*context, "body", fn, bbExit);
        auto bbTail = BasicBlock::Create(*context, "tail", fn, bbExit);
        auto bbSpill = BasicBlock::Create(*context, "spill", fn, bbExit);

        uint64_t iters = (blksize/DLJIT_VECWIDTH_2/DLJIT_UNROLL_DEGREE)*DLJIT_UNROLL_DEGREE;

        builder->SetInsertPoint(bbEntry);
        builder->CreateBr(bbHead);

        builder->SetInsertPoint(bbHead);
        Value *lsrc = builder->CreatePointerCast(src, v128Ptr);
        Value *ldst = builder->CreatePointerCast(dst, v128Ptr);
        builder->CreateBr(bbBody);

        builder->SetInsertPoint(bbBody);
        PHINode *phi = builder->CreatePHI(env->int64Ty(), 2);
        phi->addIncoming(env->int64Const(0), bbHead);
        for(int64_t i = 0; i < DLJIT_UNROLL_DEGREE; i++) {
            Value *idx = builder->CreateAdd(phi, env->int64Const(i));

            // We don't do prefetching because it doesn't seem to make a 
            // significant difference in memcpy performance. 
            Value *tmp = builder->CreateLoad(builder->CreateGEP(lsrc, idx));
            if(aligned) {
                auto finaldst = builder->CreateGEP(ldst, idx);
                builder->CreateAlignedStore(tmp, finaldst, 32);
            } else {
                auto finaldst = builder->CreateGEP(ldst, idx);
                builder->CreateStore(tmp, finaldst);
            }
        }
        builder->CreateBr(bbTail);

        builder->SetInsertPoint(bbTail);
        Value *incr = builder->CreateAdd(phi, env->int64Const(DLJIT_UNROLL_DEGREE)); 
        phi->addIncoming(incr, bbTail);
        Value *cond = builder->CreateICmpULT(incr, env->int64Const(iters)); 
        builder->CreateCondBr(cond, bbBody, bbSpill);

        builder->SetInsertPoint(bbSpill);
        uint64_t rem = blksize - iters*DLJIT_VECWIDTH_1;
        rem = gen_copy_n(env, dst, src, v128Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i64Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i32Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i16Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i8Ptr, rem, blksize-rem, aligned);
    }
    builder->CreateBr(bbExit);

    builder->SetInsertPoint(bbExit);
    builder->CreateRet(builder->CreateConstGEP1_64(dst, blksize));

    return fn;
}

Function* DLJIT_gen_memcpy_n(BuildEnv *env, int64_t blklen, int64_t oldsize, int64_t distance) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module();

    std::stringstream ss;
    ss << "my_memcpy_" << blklen << "_" << oldsize;
    std::string fname = ss.str();
    Function *fn = module->getFunction(fname);
    if(fn)
        return fn;

    std::vector<Type*> tys 
        = { env->int8PtrTy(), env->int8PtrTy() };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), tys, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));
    
    auto f_a = gen_memcpy_n(env, blklen, oldsize, distance, true);
    auto f_u = gen_memcpy_n(env, blklen, oldsize, distance, false);

    Value *dst = getFnArg(fn,0);
    Value *src = getFnArg(fn,1);

    auto bbExit = BasicBlock::Create(*context, "exit", fn);
    auto bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);
    auto bbAligned = BasicBlock::Create(*context, "aligned", fn, bbExit);
    auto bbUnaligned = BasicBlock::Create(*context, "unaligned", fn, bbExit);

    builder->SetInsertPoint(bbEntry);
    auto dstl = builder->CreatePtrToInt(dst, env->int64Ty());
    auto bits = builder->CreateAnd(dstl, env->int64Const(32)); 
    auto cond = builder->CreateICmpEQ(bits, env->int64Const(0)); 
    builder->CreateCondBr(cond, bbAligned, bbUnaligned);

    builder->SetInsertPoint(bbAligned);
    auto rv_a = builder->CreateCall2(f_a, dst, src);
    builder->CreateBr(bbExit);

    builder->SetInsertPoint(bbUnaligned);
    auto rv_u = builder->CreateCall2(f_u, dst, src);
    builder->CreateBr(bbExit);

    builder->SetInsertPoint(bbExit);
    PHINode *rv = builder->CreatePHI(env->int8PtrTy(), 2);
    rv->addIncoming(rv_a, bbAligned);
    rv->addIncoming(rv_u, bbUnaligned);
    builder->CreateRet(rv);

    return fn;
}

static void gen_copy(BuildEnv *env, Value *dst, Value *src, Value *count, int basesize, PointerType *ptrTy, BasicBlock *bbEntry, std::vector<BasicBlock*> bbs, bool aligned=false) {
    IRBuilder<> *builder = env->builder();

    auto i64Ptr = env->int64PtrTy();
    auto i32Ptr = env->int32PtrTy();
    auto i16Ptr = env->int16PtrTy();
    auto i8Ptr = env->int8PtrTy();

    Value *bytes = builder->CreateMul(count, env->int64Const(basesize));
    Value *iters = builder->CreateUDiv(bytes, env->int64Const(8));
    Value *iterbytes = builder->CreateMul(iters, env->int64Const(8));
    Value *spillStart = builder->CreateUDiv(iterbytes, env->int64Const(basesize));

    Value *dst8 = builder->CreatePointerCast(dst, env->int64PtrTy());
    Value *src8 = builder->CreatePointerCast(src, env->int64PtrTy());
    Value *dstn = builder->CreatePointerCast(dst, ptrTy);
    Value *srcn = builder->CreatePointerCast(src, ptrTy);
    SwitchInst *sw = builder->CreateSwitch(count, bbs[8], 9);
    for(int i = 1; i <= 8; i++) {
        sw->addCase(env->int64Const(i), bbs[i-1]);
        builder->SetInsertPoint(bbs[i-1]);
        long blksize = i*basesize;
        long rem = blksize;
        rem = gen_copy_n(env, dst, src, i64Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i32Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i16Ptr, rem, blksize-rem, aligned);
        rem = gen_copy_n(env, dst, src, i8Ptr, rem, blksize-rem, aligned);
        builder->CreateBr(bbs.back());
    }
    // Default case. Use wide moves as much as possible
    builder->SetInsertPoint(bbs[8]);
    PHINode *idx = builder->CreatePHI(env->int64Ty(), 2);
    idx->addIncoming(env->int64Const(0), bbEntry);
    Value *d = builder->CreateGEP(dst8, idx);
    Value *s = builder->CreateGEP(src8, idx);
    Value *tmp = builder->CreateAlignedLoad(s, 1);
    builder->CreateAlignedStore(tmp, d, 1);
    builder->CreateBr(bbs[9]);

    builder->SetInsertPoint(bbs[9]);
    Value *incr = builder->CreateAdd(idx, env->int64Const(1));
    idx->addIncoming(incr, bbs[9]);
    builder->CreateCondBr(builder->CreateICmpULT(incr,iters), bbs[8], bbs[10]);

    if(basesize < 8) {
        builder->SetInsertPoint(bbs[10]);
        builder->CreateCondBr(builder->CreateICmpEQ(spillStart, count),
                              bbs.back(), bbs[11]);

        builder->SetInsertPoint(bbs[11]);
        PHINode *idx = builder->CreatePHI(env->int64Ty(), 2);
        idx->addIncoming(spillStart, bbs[10]);
        Value *d = builder->CreateGEP(dstn, idx);
        Value *s = builder->CreateGEP(srcn, idx);
        Value *tmp = builder->CreateAlignedLoad(s, 1);
        builder->CreateAlignedStore(tmp, d, 1);
        builder->CreateBr(bbs[12]);
        
        builder->SetInsertPoint(bbs[12]);
        Value *incr = builder->CreateAdd(idx, env->int64Const(1));
        idx->addIncoming(incr, bbs[12]);
        builder->CreateCondBr(builder->CreateICmpULT(incr, count),
                              bbs[11], bbs.back());
    }
}

static std::vector<BasicBlock*> gen_bbs(BuildEnv *env, Function *fn, std::string base, BasicBlock *bbExit, bool createspill) {
    LLVMContext *context = env->context();
    std::vector<BasicBlock*> bbs;
    for(int i = 1; i <= 8; i++) {
        std::stringstream ss;
        ss << base << i;
        bbs.push_back(BasicBlock::Create(*context, ss.str(), fn, bbExit));
    }
    std::stringstream ss;
    ss << base << "_other";
    bbs.push_back(BasicBlock::Create(*context, base+"other_body", fn, bbExit));
    bbs.push_back(BasicBlock::Create(*context, base+"other_tail", fn, bbExit));
    if(createspill) {
        bbs.push_back(BasicBlock::Create(*context,base+"ck_spill",fn,bbExit));
        bbs.push_back(BasicBlock::Create(*context,base+"spill_body",fn,bbExit));
        bbs.push_back(BasicBlock::Create(*context,base+"spill_tail",fn,bbExit));
    }
    bbs.push_back(bbExit);
    return bbs;
}

/* This is the equivalent of all that nasty code in veccpy.h */
BasicBlock* DLJIT_gen_memcpy_short(BuildEnv *env, Value *dst, Value *src, Value *count, long basesize, Function *fn, BasicBlock *bbEntry, BasicBlock *bbExit) {
    /* TODO: Have an aligned version as well */
    IRBuilder<> *builder = env->builder();

    PointerType *ptrTy = env->int8PtrTy();
    switch(basesize) {
    case 8: ptrTy = env->int64PtrTy(); break;
    case 4: ptrTy = env->int32PtrTy(); break;
    case 2: ptrTy = env->int16PtrTy(); break;
    default: ptrTy = env->int8PtrTy(); break;
    }
    auto bbs = gen_bbs(env, fn, "b_", bbExit, (basesize == 8  ? false : true));

    builder->SetInsertPoint(bbEntry);
    gen_copy(env, dst, src, count, basesize, ptrTy, bbEntry, bbs, false);

    return bbEntry;
}

/* This function is to be called when both src and dst are aligned, and when
 * the stride ensures that the pointers remain aligned */
Function* DLJIT_gen_veccpy(BuildEnv *env, long blklen, long stride, long oldsize, long extent, bool pack, bool aligned) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module();

    std::stringstream ss;
    ss << "veccpy_" 
       << (pack ? "pack" : "unpack") << "_"
       << (aligned ? "aligned" : "unaligned") << "_"
       << stride << "_" << blklen;
    std::string fname = ss.str();
    Function *fn = module->getFunction(fname);
    if(fn)
        return fn;

    std::vector<Type*> param_ts 
        = { env->int8PtrTy(), /* src */
            env->int8PtrTy(), /* dst */
            env->int64Ty()    /* count */ };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), param_ts, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));
    fn->addFnAttr(Attribute::AlwaysInline);

    long blksize = blklen*oldsize;
    long prefdist_seq = DLJIT_prefetch_distance(blksize);
    long prefdist_str = DLJIT_prefetch_distance(blksize, extent, stride);

    // In case memcpy hasn't already been generated
    DLJIT_gen_memcpy_n(env, blklen, oldsize, 0);
    auto memcpy = module->getFunction(getMemcpyName(blklen, oldsize, aligned));

    Value *llvmPrefRd = env->int32Const(0);
    Value *llvmPrefWr = env->int32Const(1);
    // TODO: We should use a non-temporal level if we know that the data will 
    // not be reused. This might be the case when the array is used as part of
    // a Send and is not in a loop. 
    Value *llvmPrefLv = env->int32Const(3);
    Value *llvmPrefTy = env->int32Const(1); // Data cache
    Function *prefetch = cast<Function>(Intrinsic::getDeclaration(module, Intrinsic::prefetch));

    BasicBlock *bbExit = BasicBlock::Create(*context, "exit", fn);
    BasicBlock *bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);
    BasicBlock *bbHead = BasicBlock::Create(*context, "head", fn, bbExit);
    BasicBlock *bbBody = BasicBlock::Create(*context, "body", fn, bbExit);
    BasicBlock *bbTail = BasicBlock::Create(*context, "tail", fn, bbExit);

    /* Entry block */
    builder->SetInsertPoint(bbEntry);
    builder->CreateBr(bbHead);

    /* Loop header */
    builder->SetInsertPoint(bbHead);
    builder->CreateBr(bbBody);

    /* Loop body */
    builder->SetInsertPoint(bbBody);
    PHINode *phi = builder->CreatePHI(env->int64Ty(), 2);
    PHINode *srcPhi = builder->CreatePHI(env->int8PtrTy(), 2);
    PHINode *dstPhi = builder->CreatePHI(env->int8PtrTy(), 2);
    phi->addIncoming(env->int64Const(0), bbHead);
    dstPhi->addIncoming(getFnArg(fn, 0), bbHead);
    srcPhi->addIncoming(getFnArg(fn, 1), bbHead);

    if(prefdist_seq > 0) {
        for(long i = 0; i < blksize and i < DLJIT_CACHE_LINE_SIZE * 8; i += DLJIT_CACHE_LINE_SIZE) {
            Value *idx = builder->CreateAdd(env->int64Const(prefdist_seq),
                                            env->int64Const(i));
            if(pack) {
                Value *ptr = builder->CreateGEP(dstPhi, idx);
                builder->CreateCall4(prefetch, 
                                     ptr, llvmPrefWr, llvmPrefLv, llvmPrefTy);
            } else {
                Value *ptr = builder->CreateGEP(srcPhi, idx);
                builder->CreateCall4(prefetch, 
                                     ptr, llvmPrefRd, llvmPrefLv, llvmPrefTy);
            }
        } 
    }
    if(prefdist_str > 0) {
        for(long i = 0; i < blksize and i < DLJIT_CACHE_LINE_SIZE * 8; i+= DLJIT_CACHE_LINE_SIZE) {
            Value *idx = builder->CreateAdd(env->int64Const(prefdist_str),
                                            env->int64Const(i));
            if(pack) {
                Value *ptr = builder->CreateGEP(srcPhi, idx);
                builder->CreateCall4(prefetch,
                                     ptr, llvmPrefRd, llvmPrefLv, llvmPrefTy);
            } else {
                Value *ptr = builder->CreateGEP(dstPhi, idx);
                builder->CreateCall4(prefetch,
                                     ptr, llvmPrefWr, llvmPrefLv, llvmPrefTy);
            }
        }
    }
    builder->CreateCall2(memcpy, dstPhi, srcPhi, "next");
    builder->CreateBr(bbTail);
    
    /* Outer loop tail */
    builder->SetInsertPoint(bbTail);
    Value *incr = builder->CreateAdd(phi, env->int64Const(1));
    Value *incrSrc = nullptr, *incrDst = nullptr;
    if(pack) {
        incrSrc = builder->CreateConstGEP1_64(srcPhi, stride);
        incrDst = builder->CreateConstGEP1_64(dstPhi, blksize);
    } else {
        incrSrc = builder->CreateConstGEP1_64(srcPhi, blksize);
        incrDst = builder->CreateConstGEP1_64(dstPhi, stride);
    }
    phi->addIncoming(incr, bbTail);
    srcPhi->addIncoming(incrSrc, bbTail);
    dstPhi->addIncoming(incrDst, bbTail);
    Value *cond = builder->CreateICmpSLT(incr, getFnArg(fn, 2));
    builder->CreateCondBr(cond, bbBody, bbExit);

    /* Exit block */
    builder->SetInsertPoint(bbExit);
    if(pack) 
        builder->CreateRet(incrDst);
    else 
        builder->CreateRet(incrSrc);

    return fn;
}
