#include "jit.h"

#include "llvm/ADT/Triple.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/FormattedStream.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

#include "llvm/PassManager.h"
#include "llvm/LinkAllPasses.h"

#include <sstream>
#include <time.h>

// TODO: Should not include this unless necessary
#include "debug.h"

using namespace llvm;

TargetLibraryInfo *TLI;
TargetMachine *TM;

Argument* getFnArg(Function *fn, unsigned idx) {
    auto it = fn->arg_begin();
    while(idx-- > 0)
        it++;
    return it;
}


static void addOptimizationPasses(bool unroll, bool vectorize, unsigned OptLevel, unsigned SizeLevel, FunctionPassManager *FPM, PassManager *MPM) {
    FPM->add(createVerifierPass());
    
    PassManagerBuilder Builder;
    Builder.OptLevel = OptLevel;
    Builder.SizeLevel = SizeLevel;
    
    if (OptLevel > 1) {
        unsigned Threshold = 225;
        if (SizeLevel == 1)      // -Os
            Threshold = 75;
        else if (SizeLevel == 2) // -Oz
            Threshold = 25;
        if (OptLevel > 2)
            Threshold = 275;
        Builder.Inliner = createFunctionInliningPass(Threshold);
    } else {
        Builder.Inliner = createAlwaysInlinerPass();
    }

    Builder.DisableUnrollLoops = not unroll;
    Builder.LoopVectorize = vectorize;
    Builder.SLPVectorize = vectorize;
    
    Builder.populateFunctionPassManager(*FPM);
    Builder.populateModulePassManager(*MPM);
}

static void addPassesForMPIDatatypes(FunctionPassManager *FPM) {
    FPM->add(createBasicAliasAnalysisPass());
    FPM->add(createPromoteMemoryToRegisterPass());
    FPM->add(createCFGSimplificationPass());
    FPM->add(createInstructionCombiningPass());
    FPM->add(createGVNPass());
    FPM->add(createCFGSimplificationPass());
    FPM->add(createLCSSAPass());
    FPM->add(createLoopSimplifyPass());
    FPM->add(createIndVarSimplifyPass());
    FPM->add(createLoopUnrollPass());
    FPM->add(createInstructionCombiningPass());
    FPM->add(createAggressiveDCEPass());
    FPM->doInitialization();
}

BuildEnv::BuildEnv(Module *module, LLVMContext *ctxt, const Dataloop *dl) 
    : _module(module), _dl(dl), _ctxt(ctxt), _builder(new IRBuilder<>(*ctxt)),
      _resumeBlocks(decltype(_resumeBlocks)()) {
    /* FIXME: It is redundant to have to create these types twice, 
       but they're not too expensive */
    std::vector<Type*> selm_ts={ int8PtrTy(), int8PtrTy(), int64Ty() };
    StructType *selmTy = module->getTypeByName("stackelm_llvm");
    if(!selmTy)
        selmTy = StructType::create(*_ctxt, selm_ts, "stackelm_llvm");

    ArrayType *stackTy = ArrayType::get(selmTy, DLOOP_MAX_DATALOOP_STACK);
    std::vector<Type*> state_ts = { int64Ty(), int8PtrTy(), int64Ty(), stackTy};
    _stateType = module->getTypeByName("state_llvm");
    if(!_stateType)
        _stateType = StructType::create(*_ctxt, state_ts, "state_llvm");
}

BuildEnv::~BuildEnv() {
    delete _builder;
    for(auto sv : _sltStackVars)
        delete sv;
}

void BuildEnv::initStackVars(Function *fn, BasicBlock *bbEntry) {
    _currfn = fn;

    /* Create stack variables */
    _builder->SetInsertPoint(bbEntry);
 
    _inbase = _builder->CreateAlloca(int8PtrTy(), nullptr, "inbase");
    _outbase = _builder->CreateAlloca(int8PtrTy(), nullptr, "outbase");
    _sizeLeft = _builder->CreateAlloca(int64Ty(), nullptr, "sizeLeft");
    _partial = _builder->CreateAlloca(int64Ty(), nullptr, "partial");
 
    Value *state = getFnArg(fn, PARAM_STATE);
    Value *partial = _builder->CreateLoad(_builder->CreateStructGEP(state, STATE_PARTIAL_IDX));
    _builder->CreateStore(partial, _partial);
    _stack = _builder->CreateStructGEP(state, STATE_STACK_IDX);
}

unsigned BuildEnv::SLTCreate(long count) {
    ArrayType *type = ArrayType::get(int8PtrTy(), count);
    std::stringstream ss;

    unsigned key = _slt.size();
    _slt[key] = std::vector<llvm::BasicBlock*>();
    ss << "slt" << key;
    _sltStackVars.push_back(new AllocaInst(type, ss.str()));
    return key;
}

bool BuildEnv::SLTAdd(unsigned key, llvm::BasicBlock *bb) {
    if(_slt.find(key) != _slt.end()) {
        _slt[key].push_back(bb);
        return true;
    } 
    return false;
}

std::vector<llvm::BasicBlock*> BuildEnv::SLTGetBlocks(unsigned key) const {
    if(_slt.find(key) != _slt.end())
        return _slt.at(key);
    return std::vector<llvm::BasicBlock*>();
}

llvm::BasicBlock* BuildEnv::SLTLookup(unsigned key, unsigned idx) const {
    if(_slt.find(key) != _slt.end() and _slt.at(key).size() > idx)
        return _slt.at(key).at(idx);
    return nullptr;
}

llvm::AllocaInst* BuildEnv::SLTGetStackVar(unsigned key) const {
    if(_sltStackVars.size() > key)
        return _sltStackVars.at(key);
    return nullptr;
}

static BasicBlock* DLJIT_gen_init_slts(BuildEnv *env, BasicBlock *bb) {
    IRBuilder<> *builder = env->builder();
    Module *module = env->module();
    Function *fn = env->fn();
    
    builder->SetInsertPoint(bb);
    for(unsigned i = 0; i < env->SLTGetNumStackVars(); i++) {
        std::vector<BasicBlock*> blocks = env->SLTGetBlocks(i);
        std::vector<Constant*> inits;
        for(auto block : blocks)
            inits.push_back(BlockAddress::get(fn, block));
        ArrayType *ty = ArrayType::get(env->int8PtrTy(), blocks.size());
        Constant *c = ConstantArray::get(ty, inits);
        GlobalVariable *gv = new GlobalVariable(*module, ty, true, GlobalValue::LinkageTypes::PrivateLinkage, c, "str");
        env->SLTGetStackVar(i)->replaceAllUsesWith(gv);
    }

    return bb;
}

static BasicBlock* DLJIT_gen_header(BuildEnv *env, BasicBlock *bbEntry, BasicBlock *bb, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module();
    Function *fn = env->fn();

    Value *stack = env->stack();

    BasicBlock *bbResume = BasicBlock::Create(*context, "resume", fn, bb);

    /* Check if the call is a resume */
    builder->SetInsertPoint(bbEntry);
    builder->CreateStore(getFnArg(fn, PARAM_IN), env->inbase());
    builder->CreateStore(getFnArg(fn, PARAM_OUT), env->outbase());
    builder->CreateStore(getFnArg(fn, PARAM_SIZE), env->sizeLeft());
    DLJIT_gen_init_slts(env, bbEntry);

    Value *state = getFnArg(fn, PARAM_STATE);
    Value *spGEP = builder->CreateStructGEP(state, STATE_SP_IDX);
    Value *sp = builder->CreateLoad(spGEP);
    Value *isResumed = builder->CreateICmpNE(sp, env->int64Const(0));
    builder->CreateCondBr(isResumed, bbResume, bb);
    
    // Initializations when resuming
    builder->SetInsertPoint(bbResume);

    // Restore base
    std::vector<Value*> idxs = { env->int64Const(0), sp };
    Value *selm = builder->CreateGEP(stack, idxs);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    if(pack)
        builder->CreateStore(builder->CreateLoad(baseGEP), env->inbase());
    else
        builder->CreateStore(builder->CreateLoad(baseGEP), env->outbase());

    // Jump to the right block
    Value *dlGEP = builder->CreateStructGEP(state, STATE_DL_IDX);
    // We have, unfortunately, saved an integer value into an int* field
    Value *idx = builder->CreatePtrToInt(builder->CreateLoad(dlGEP), 
                                         env->int64Ty());

    std::vector<Constant*> cs;
    for(auto block : env->resumeBlocks())
        cs.push_back(BlockAddress::get(fn, block));
    ArrayType *ty = ArrayType::get(env->int8PtrTy(), env->numResumeBlocks());
	Constant *c = ConstantArray::get(ty, cs);
	GlobalVariable *gv = new GlobalVariable(*module, ty, true, GlobalValue::LinkageTypes::PrivateLinkage, c, (pack ? "resume_p" : "resume_u"));
    
    std::vector<Value*> indices = { env->int64Const(0), idx };
    Value *jmpGEP = builder->CreateInBoundsGEP(gv, indices);
    Value *jmpBlock = builder->CreateLoad(jmpGEP);
    auto ibr = builder->CreateIndirectBr(jmpBlock, env->numResumeBlocks());
    for(auto block : env->resumeBlocks())
        ibr->addDestination(block);

    return bbEntry;
}

static int DLJIT_gen_pack(Module *module, LLVMContext *context, const Dataloop *dl) {
    BuildEnv *env = new BuildEnv(module, context, dl);
    IRBuilder<> *builder = env->builder();

    std::vector<Type*> param_ts
        = { env->int8PtrTy(),  /* inPtr */
            env->int8PtrTy(),  /* outPtr */
            env->int64Ty(),    /* outSize */
            env->statePtrTy(), /* state_p */
            env->int64PtrTy()  /* copy_p */ };
    FunctionType *fnType = FunctionType::get(env->int32Ty(), param_ts, false);
    /* TODO: TLP: Add attributes to the function */
    auto fn = cast<Function>(module->getOrInsertFunction("dl_pack", fnType));

    BasicBlock *bbExit = BasicBlock::Create(*context, "exit", fn);
    BasicBlock *bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);

    env->initStackVars(fn, bbEntry);
    BasicBlock *bb =  DLJIT_gen_loop(env, 1, bbExit, bbExit, true);
    if(!bb)
        return -1;
    if(!DLJIT_gen_header(env, bbEntry, bb, true))
        return -1;

    /* Populate exit block */
    builder->SetInsertPoint(bbExit);
    Value *state = getFnArg(fn, PARAM_STATE);
    Value *spGEP = builder->CreateStructGEP(state, STATE_SP_IDX);
    builder->CreateStore(env->int64Const(0), spGEP);
    Value *dlGEP = builder->CreateStructGEP(state, STATE_DL_IDX);
    builder->CreateStore(ConstantPointerNull::get(env->int8PtrTy()), dlGEP);
    Value *copied = builder->CreateSub(getFnArg(fn, PARAM_SIZE),
                                       builder->CreateLoad(env->sizeLeft()));
    builder->CreateStore(copied, getFnArg(fn, PARAM_COPIED));
    builder->CreateRet(ConstantInt::get(env->int32Ty(), 0));

    delete env;
    return 0;
}

static int DLJIT_gen_unpack(Module *module, LLVMContext *context, const Dataloop *dl) {
    BuildEnv *env = new BuildEnv(module, context, dl);
    IRBuilder<> *builder = env->builder();

    std::vector<Type*> param_ts
        = { env->int8PtrTy(),  /* inPtr */
            env->int8PtrTy(),  /* outPtr */
            env->int64Ty(),    /* outSize */
            env->statePtrTy(), /* state_p */
            env->int64PtrTy()  /* copy_p */ };
    FunctionType *fnType = FunctionType::get(env->int32Ty(), param_ts, false);
    /* TODO: TLP: Add attributes to the function */
    auto fn = cast<Function>(module->getOrInsertFunction("dl_unpack",fnType));

    BasicBlock *bbExit = BasicBlock::Create(*context, "exit", fn);
    BasicBlock *bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);

    env->initStackVars(fn, bbEntry);
    BasicBlock *bb = DLJIT_gen_loop(env, 1, bbExit, bbExit, false);
    if(!bb)
        return -1;
    if(!DLJIT_gen_header(env, bbEntry, bb, false))
        return -1;

    /* Populate exit block */
    builder->SetInsertPoint(bbExit);
    Value *state = getFnArg(fn, PARAM_STATE);
    Value *spGEP = builder->CreateStructGEP(state, STATE_SP_IDX);
    builder->CreateStore(env->int64Const(0), spGEP);
    Value *dlGEP = builder->CreateStructGEP(state, STATE_DL_IDX);
    builder->CreateStore(ConstantPointerNull::get(env->int8PtrTy()), dlGEP);
    Value *copied = builder->CreateSub(getFnArg(fn, PARAM_SIZE),
                                       builder->CreateLoad(env->sizeLeft()));
    builder->CreateStore(copied, getFnArg(fn, PARAM_COPIED));
    builder->CreateRet(ConstantInt::get(env->int32Ty(), 0));

    delete env;
    return 0;
}

static Module* DLJIT_build_module(const Dataloop *dl, LLVMContext *context) {
    Module *module = new Module("dlengine", *context);
    module->setTargetTriple(MACHINE_TRIPLE);
    module->setDataLayout(MACHINE_DATALAYOUT);

    auto okPack = DLJIT_gen_pack(module, context, dl);
    auto okUnpack = DLJIT_gen_unpack(module, context, dl);
    if(not (okPack == 0 and okUnpack == 0))
        return nullptr;

    PassManager *MPM = new PassManager();
    FunctionPassManager *FPM = new FunctionPassManager(module);
    MPM->add(TLI);
    MPM->add(new DataLayoutPass());
    FPM->add(new DataLayoutPass());
    if(TM) {
        TM->addAnalysisPasses(*MPM);
#ifdef PRINT_ASM
        raw_fd_ostream *fd = new raw_fd_ostream(2, false);
        formatted_raw_ostream *fos = new formatted_raw_ostream(*fd);
        TM->addPassesToEmitFile(*MPM, 
                                *fos,
                                CodeGenFileType::CGFT_AssemblyFile);
#endif
    }
    addOptimizationPasses(true, true, 2, 0, FPM, MPM);
    for(auto f = module->begin(); f != module->end(); f++)
        FPM->run(*f);
    MPM->run(*module);

    return module;
}

DLJIT_wrapper* DLJIT_compile_all(const Dataloop *dl) {
    LLVMContext *context = new LLVMContext();
    if(Module *module = DLJIT_build_module(dl, context)) {
        TargetOptions TO;
        TO.JITEmitDebugInfo = 0;
        ExecutionEngine *ee = EngineBuilder(std::move(std::unique_ptr<Module>(module)))
            .setEngineKind(EngineKind::JIT)
            .setOptLevel(CodeGenOpt::Aggressive)
            .setTargetOptions(TO)
            .create();
        ee->finalizeObject();
        return new DLJIT_wrapper(ee, context);
    }
    return nullptr;
}

BasicBlock* DLJIT_gen_loop(BuildEnv *env, int i, BasicBlock* bbPrev, BasicBlock *bbExit, bool pack) {
    switch(env->dl()[i].kind) {
    case DL_CONTIG:
        return DLJIT_contiguous(env, i, bbPrev, bbExit, pack);
        break;
    case DL_VECTOR:
    case DL_VECTOR1:
        return DLJIT_vector(env, i, bbPrev, bbExit, pack);
        break;
    case DL_BLOCKINDEX:
    case DL_BLOCKINDEX1:
        return DLJIT_blockindexed(env, i, bbPrev, bbExit, pack);
        break;
    case DL_INDEX:
        return DLJIT_indexed(env, i, bbPrev, bbExit, pack);
        break;
    case DL_STRUCT:
        return DLJIT_struct(env, i, bbPrev, bbExit, pack);
        break;
    case DL_VECTORFINAL:
        return DLJIT_vectorfinal(env, i, bbPrev, bbExit, pack);
        break;
    case DL_BLOCKINDEXFINAL:
        return DLJIT_blockindexedfinal(env, i, bbPrev, bbExit, pack);
        break;
    case DL_INDEXFINAL:
        return DLJIT_indexedfinal(env, i, bbPrev, bbExit, pack);
        break;
    case DL_CONTIGCHILD:
        return DLJIT_contigchild(env, i, bbPrev, bbExit, pack);
        break;
    case DL_CONTIGFINAL:
        return DLJIT_contigfinal(env, i, bbPrev, bbExit, pack);
        break;
    case DL_RETURNTO:
        return DLJIT_returnto(env, i, bbPrev, bbExit, pack);
        break;
    case DL_BOTTOM:
    case DL_EXIT:
    default:
        return nullptr;
        break;
    }
    return nullptr;
}

