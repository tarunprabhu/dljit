#include "jit.h"
#include <sstream>
#include <iostream>

using namespace llvm;


static BasicBlock* DLJIT_vec_push(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbInner, bool pack) {
    IRBuilder<> *builder = env->builder();
    const Dataloop *dl = env->dl();

    builder->SetInsertPoint(bb);
    Value *base = nullptr;
    if(pack) 
        base = builder->CreateLoad(env->inbase());
    else
        base = builder->CreateLoad(env->outbase());

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmBlklen = env->int64Const(dl[sp].s.v_t.blklen);

    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    builder->CreateStore(base, baseGEP);        // stack[sp].base = base
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmCount, leftGEP);   // stack[sp].left = count

    if(dl[sp].kind != DL_VECTOR1) {
        Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
        Value *base1GEP = builder->CreateStructGEP(selm1, STACK_BASE_IDX);
        builder->CreateStore(base, base1GEP);       // stack[sp+1].base = base
        Value *left1GEP=builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
        builder->CreateStore(llvmBlklen, left1GEP); // stack[sp+1].left = blklen
    }

    builder->CreateBr(bbInner);

    return bb;
}

static BasicBlock *DLJIT_vec_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    const Dataloop *dl = env->dl();
    Function *fn = env->fn();
    std::stringstream ss;

    Value *stack = env->stack();

    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmBlklen = env->int64Const(dl[sp].s.v_t.blklen);
    Constant *llvmStride = env->int64Const(dl[sp].s.v_t.stride);

    ss.str(std::string());
    ss << "vec" << sp << "_done";
    BasicBlock *bbDone = BasicBlock::Create(*context, ss.str(), fn, bbIns);
    ss.str(std::string());
    ss << "vec" << sp << "_notdone";
    BasicBlock *bbNotDone = BasicBlock::Create(*context, ss.str(), fn, bbIns);
    
    /* Check if the type is done */
    builder->SetInsertPoint(bb);
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    // if(--stack[sp].countLeft == 0)
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX); 
    Value *left = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(left, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbNotDone);

    /* Not done with the type */
    builder->SetInsertPoint(bbNotDone);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *base = builder->CreateLoad(baseGEP);
    
    Value *idx = builder->CreateSub(llvmCount, newLeft);
    Value *offset = builder->CreateMul(idx, llvmStride);
    Value *newBase = builder->CreateGEP(base, offset);
    if(pack) 
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());

    if(dl[sp].kind != DL_VECTOR1) {
        Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
        Value *left1GEP=builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
        builder->CreateStore(llvmBlklen, left1GEP); // stack[sp+1].left = blklen
    }

    builder->CreateBr(bbInner);

    /* Finished with the type */
    builder->SetInsertPoint(bbDone);
    builder->CreateBr(bbOuter);

    return bb;
}

BasicBlock* DLJIT_vector(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    std::stringstream ss;

    ss.str(std::string());
    ss << "vec" << sp << "_pop";
    BasicBlock *bbPop = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) return nullptr;

    ss.str(std::string());
    ss << "vec" << sp << "_push";
    BasicBlock *bbPush = BasicBlock::Create(*context, ss.str(), fn, bbInner);

    auto rvPop = DLJIT_vec_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_vec_push(env, sp, bbPush, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

/* NOTE: This function returns the last basic block before which it has to 
 * jump to the contigfinal block. This is different from most of the other 
 * functions which return the entry block of the corresponding instruction. 
 * The jump to contigfinal will be inserted after the contigfinal block has been
 * generated (which will be done once pop has been codegen-ed */
static BasicBlock* DLJIT_vecfinal_push(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();

    const Dataloop *dl = env->dl();
    Function *fn = env->fn();

    long size = dl[sp].size;
    long count = dl[sp].count;
    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    long stride = dl[sp].s.v_t.stride;
    long blklen = dl[sp].s.v_t.blklen;
    long oldsize = dl[sp].s.v_t.oldsize;

    Constant *llvmCount = env->int64Const(count);
    Constant *llvmSize = env->int64Const(size);
    Constant *llvmStride = env->int64Const(stride);
    Constant *llvmBlklen = env->int64Const(blklen);
    Constant *llvmBlksize = env->int64Const(blklen*oldsize);

    BasicBlock *bbAll = BasicBlock::Create(*context, "vf_push_all", fn, bbIns);
    BasicBlock *bbCountCheck = BasicBlock::Create(*context,"vf_push_ck_count", fn, bbIns);
    BasicBlock *bbSome = BasicBlock::Create(*context,"vf_push_some", fn, bbIns);
    BasicBlock *bbNone = BasicBlock::Create(*context,"vf_push_none", fn, bbIns);

    Value *stack = env->stack();
    Function *packAll = DLJIT_gen_vecfinal(env, count, blklen, stride, oldsize, extent, flags, pack);
    Function *packSome = DLJIT_gen_vecfinal(env, blklen, stride, oldsize, extent, flags, pack);


    /* Check how many elements can be copied */
    builder->SetInsertPoint(bb);
    builder->CreateAlloca(env->int64Ty(), nullptr, "idx");
    Value *inbase = builder->CreateLoad(env->inbase());
    Value *outbase = builder->CreateLoad(env->outbase());
    Value *sizeleft = builder->CreateLoad(env->sizeLeft());

    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    if(pack)
        builder->CreateStore(inbase, baseGEP);
    else 
        builder->CreateStore(outbase, baseGEP);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmCount, leftGEP);
    Value *enough = builder->CreateICmpSGE(sizeleft, llvmSize);
    builder->CreateCondBr(enough, bbAll, bbCountCheck);
                          
    /* Enough space for the full vector */
    builder->SetInsertPoint(bbAll);
    Value *newOutbaseAll = nullptr, *newInbaseAll = nullptr;
    if(pack) {
        newOutbaseAll = builder->CreateCall2(packAll, outbase, inbase);
        newInbaseAll = builder->CreateConstGEP1_64(inbase, extent);
    } else {
        newInbaseAll = builder->CreateCall2(packAll, outbase, inbase);
        newOutbaseAll = builder->CreateConstGEP1_64(outbase, extent);
    }
    builder->CreateStore(newOutbaseAll, env->outbase());
    builder->CreateStore(newInbaseAll, env->inbase());
    builder->CreateStore(env->int64Const(0), leftGEP);
    Value *newSizeLeftAll = builder->CreateSub(sizeleft, llvmSize);
    builder->CreateStore(newSizeLeftAll, env->sizeLeft());
    
    builder->CreateBr(bbOuter);
    
    /* Check how many elements can be copied */
    builder->SetInsertPoint(bbCountCheck);
    Value *ct = builder->CreateSDiv(sizeleft, llvmBlksize);
    builder->CreateCondBr(builder->CreateICmpSGT(ct, env->int64Const(0)),
                          bbSome, bbNone);

    /* Some elements can be copied */
    builder->SetInsertPoint(bbSome);
    Value *newBaseSome = builder->CreateCall3(packSome, outbase, inbase, ct);
    if(pack)
        builder->CreateStore(newBaseSome, env->outbase());
    else
        builder->CreateStore(newBaseSome, env->inbase());
    Value *copied = builder->CreateMul(ct, llvmBlksize);
    Value *newSizeLeftSome = builder->CreateSub(sizeleft, copied);
    builder->CreateStore(newSizeLeftSome, env->sizeLeft());
    Value *leftGEPSome = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    Value *oldLeftSome = builder->CreateLoad(leftGEPSome);
    Value *newLeftSome = builder->CreateSub(oldLeftSome, ct);
    builder->CreateStore(newLeftSome, leftGEPSome);
    builder->CreateBr(bbNone);

    /* Not even a single block can be copied */
    builder->SetInsertPoint(bbNone);
    {
    Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *left1GEP = builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
    builder->CreateStore(llvmBlklen, left1GEP);
    Value *newBaseNone = builder->CreateGEP(builder->CreateLoad(baseGEP),
                                            builder->CreateMul(ct,llvmStride));
    if(pack)
        builder->CreateStore(newBaseNone, env->inbase());
    else
        builder->CreateStore(newBaseNone, env->outbase());
    builder->CreateBr(bbInner);
    }    

    return bb;
}

static BasicBlock* DLJIT_vecfinal_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    const Dataloop *dl = env->dl();
    Function *fn = env->fn();

    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    long stride = dl[sp].s.v_t.stride;
    long blklen = dl[sp].s.v_t.blklen;
    long oldsize = dl[sp].s.v_t.oldsize;

    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmStride = env->int64Const(stride);
    Constant *llvmBlklen = env->int64Const(blklen);
    Constant *llvmBlksize = env->int64Const(blklen*oldsize);

    Function *packSome = DLJIT_gen_vecfinal(env, blklen, stride, oldsize, extent, flags, pack);

    auto bbCountCheck = BasicBlock::Create(*context,"vf_pop_ck_count", fn, bbIns);
    auto bbCountGT0 = BasicBlock::Create(*context, "vf_count_gt0", fn, bbIns);
    auto bbSome = BasicBlock::Create(*context,"vf_pop_some", fn, bbIns);
    auto bbNone = BasicBlock::Create(*context,"vf_pop_none", fn, bbIns);
    auto bbDone = BasicBlock::Create(*context, "vf_done", fn, bbIns);

    /* Entry block for pop */
    builder->SetInsertPoint(bb);
    AllocaInst *idx = builder->CreateAlloca(env->int64Ty(), nullptr, "idx");
    AllocaInst *count = builder->CreateAlloca(env->int64Ty(), nullptr, "count");
    Value *sizeLeft = builder->CreateLoad(env->sizeLeft());
    Value *stack = env->stack();
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *currBase = builder->CreateLoad(baseGEP);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    Value *currLeft = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(currLeft, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbCountCheck);

    /* Check count */
    builder->SetInsertPoint(bbCountCheck);
    builder->CreateStore(builder->CreateSub(llvmCount, newLeft), idx);
    builder->CreateStore(builder->CreateSDiv(sizeLeft, llvmBlksize), count);
    Value *cnt = builder->CreateLoad(count);
    Value *cmpCountGT0 = builder->CreateICmpSGT(cnt, env->int64Const(0));
    builder->CreateCondBr(cmpCountGT0, bbCountGT0, bbNone);

    /* count > 0 */
    builder->SetInsertPoint(bbCountGT0);
    Value *newCnt = builder->CreateSelect(builder->CreateICmpSGT(cnt, newLeft), 
                                          newLeft, cnt);
    builder->CreateStore(newCnt, count);
    builder->CreateBr(bbSome);

    /* Some can be copied */

    builder->SetInsertPoint(bbSome);
    {
    Value *ct = builder->CreateLoad(count);
    Value *offset = builder->CreateMul(builder->CreateLoad(idx), llvmStride);
    if(pack) {
        Value *base = builder->CreateGEP(currBase, offset);
        Value *outbase = builder->CreateLoad(env->outbase());
        builder->CreateStore(builder->CreateCall3(packSome, outbase, base, ct), 
                             env->outbase());
    } else {
        Value *inbase = builder->CreateLoad(env->inbase());
        Value *base = builder->CreateGEP(currBase, offset);
        builder->CreateStore(builder->CreateCall3(packSome, base, inbase, ct), 
                             env->inbase());
    }
    Value *copied = builder->CreateMul(ct, llvmBlksize);
    Value *newSizeLeft = builder->CreateSub(sizeLeft, copied);
    builder->CreateStore(newSizeLeft, env->sizeLeft());
    Value *currLeft = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(currLeft, ct);
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft,env->int64Const(0)),
                          bbDone, bbNone);
    }

    /* Done */
    builder->SetInsertPoint(bbDone);
    {
    Value *newBase = builder->CreateConstGEP1_64(currBase, extent);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else
        builder->CreateStore(newBase, env->outbase());
    builder->CreateBr(bbOuter);
    }
    
    /* None can be copied */
    builder->SetInsertPoint(bbNone);
    {
    Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *left1GEP = builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
    builder->CreateStore(llvmBlklen, left1GEP);
    Value *newidx = builder->CreateAdd(builder->CreateLoad(idx), 
                                       builder->CreateLoad(count));
    Value *offset = builder->CreateMul(newidx, llvmStride);
    Value *newBase = builder->CreateGEP(currBase, offset);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else
        builder->CreateStore(newBase, env->outbase());
    builder->CreateBr(bbInner);
    }
   
    return bb;
}

BasicBlock* DLJIT_vectorfinal(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();

    BasicBlock *bbPop = BasicBlock::Create(*context, "vf_pop", fn, bbIns);
    BasicBlock *bbInner = DLJIT_contigfinal(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner)
        return nullptr;
    BasicBlock *bbPush = BasicBlock::Create(*context,"vf_push", fn, bbInner);
    auto rvPop = DLJIT_vecfinal_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_vecfinal_push(env, sp, bbPush, bbOuter, bbInner, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}
