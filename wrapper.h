#ifndef TLP_DATATYPES_DLJIT_WRAPPER_H
#define TLP_DATATYPES_DLJIT_WRAPPER_H

#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/IR/LLVMContext.h"

#include "types.h"

class DLJIT_wrapper {
private:
    llvm::ExecutionEngine *ee;
    llvm::LLVMContext *c;
    DLJIT_function_ptr packEntry, unpackEntry;
public:
    DLJIT_wrapper(llvm::ExecutionEngine *ee, llvm::LLVMContext *c);
    ~DLJIT_wrapper();

    DLJIT_function_ptr getPack()   const;
    DLJIT_function_ptr getUnpack() const;
};


#endif
