#ifndef TLP_DATATYPES_MYJIT_TYPES_H
#define TLP_DATATYPES_MYJIT_TYPES_H

/* The ifdef is here so that we only include the function pointer typedef 
 * in dljit.h when it is included inside MPICH */
#ifdef __cplusplus

typedef int(*DLJIT_function_ptr)(void*, void*, long, void*, long*);
typedef unsigned char byte;

typedef struct EncodeMetadataT {
    unsigned short memidx;
    unsigned short regidx;
    unsigned short immidx;
    bool islookup;
} EncodeMetadata;

#else 

typedef int(*DLJIT_function_ptr)(void*, void*, long, void*, long*);

#endif

#endif
