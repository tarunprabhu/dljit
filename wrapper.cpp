#include "wrapper.h"

using namespace llvm;

DLJIT_wrapper::DLJIT_wrapper(ExecutionEngine *_ee, LLVMContext *_c)
    : ee(_ee), c(_c) {
    Function *fnPack = ee->FindFunctionNamed("dl_pack");
    packEntry = (DLJIT_function_ptr)(ee->getPointerToFunction(fnPack));

    Function *fnUnpack = ee->FindFunctionNamed("dl_unpack");
    unpackEntry = (DLJIT_function_ptr)(ee->getPointerToFunction(fnUnpack));
}

DLJIT_wrapper::~DLJIT_wrapper() {
    delete ee;
}

DLJIT_function_ptr DLJIT_wrapper::getPack() const {
    return packEntry;
}

DLJIT_function_ptr DLJIT_wrapper::getUnpack() const {
    return unpackEntry;
}
