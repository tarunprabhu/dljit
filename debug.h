#ifndef DLJIT_DEBUG_H
#define DLJIT_DEBUG_H

#define DBG_PRINTF(v)                                                   \
    /* Start of printf */                                               \
    {                                                                   \
        Module *module = env->module();                                 \
        LLVMContext *context = env->context();                          \
        ArrayType* arrTy = ArrayType::get(IntegerType::get(*context, 8), 5); \
        Constant *str = ConstantDataArray::getString(*context, "%ld\x0A", true); \
        GlobalVariable* gstr                                            \
            = new GlobalVariable(*env->module(),                        \
                                 arrTy,                                 \
                                 true,                                  \
                                 GlobalValue::PrivateLinkage,           \
                                 0,                                     \
                                 ".str");                               \
        gstr->setAlignment(1);                                          \
        gstr->setInitializer(str);                                      \
        std::vector<Type*> paramTys = { gstr->getType() };              \
        FunctionType *fnTy = FunctionType::get(env->int32Ty(), paramTys, true);\
        Function *printfFn = cast<Function>(module->getOrInsertFunction("printf", fnTy)); \
        builder->CreateCall2(printfFn, gstr, (v));                      \
    }                                                                   \
        /* End of printf */                                             \


#define DBG_PRINTF2(v1, v2)                                             \
    /* Start of printf */                                               \
    {                                                                   \
        Module *module = env->module();                                 \
        LLVMContext *context = env->context();                          \
        ArrayType* arrTy = ArrayType::get(IntegerType::get(*context, 8), 9); \
        Constant *str = ConstantDataArray::getString(*context, "%ld %ld\x0A", true); \
        GlobalVariable* gstr                                            \
            = new GlobalVariable(*env->module(),                        \
                                 arrTy,                                 \
                                 true,                                  \
                                 GlobalValue::PrivateLinkage,           \
                                 0,                                     \
                                 ".str");                               \
        gstr->setAlignment(1);                                          \
        gstr->setInitializer(str);                                      \
        std::vector<Type*> paramTys = { gstr->getType() };              \
        FunctionType *fnTy = FunctionType::get(env->int32Ty(), paramTys, true); \
        Function *printfFn = cast<Function>(module->getOrInsertFunction("printf", fnTy)); \
        builder->CreateCall3(printfFn, gstr, (v1), (v2));               \
    }                                                                   \
        /* End of printf */                                             \


#define DBG_PRINTF_ENV(env, v)                                          \
    /* Start of printf */                                               \
    {                                                                   \
        IRBuilder<> *builder = (env)->builder();                        \
        LLVMContext *context = (env)->context();                        \
        Module *module = (env)->module();                               \
        ArrayType* arrTy = ArrayType::get(IntegerType::get(*context, 8), 5); \
        Constant *str = ConstantDataArray::getString(*context, "%ld\x0A", true); \
        GlobalVariable* gstr                                            \
            = new GlobalVariable(*(env)->module(),                      \
                                 arrTy,                                 \
                                 true,                                  \
                                 GlobalValue::PrivateLinkage,           \
                                 0,                                     \
                                 ".str");                               \
        gstr->setAlignment(1);                                          \
        gstr->setInitializer(str);                                      \
        std::vector<Type*> paramTys = { gvar_array__str->getType() };   \
        FunctionType *fnTy = FunctionType::get((env)->int32Ty(), paramTys, true); \
        Function *printfFn = cast<Function>(module->getOrInsertFunction("printf", fnTy)); \
        builder->CreateCall2(printfFn, gstr, (v));                      \
    }                                                                   \
        /* End of printf */                                             \

#endif
