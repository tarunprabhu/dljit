#include <stdint.h>

#include "jit.h"

/* We use a simple heuristic to calculate the prefetch distance. 
 *
 * distance = (K * L_n)/(n_{mem}*L_1 + n_o*CPI)
 * 
 * Where K is a constant. Here we choose K = 100.
 * L_n is the latency to cache level n
 * n_{mem} is the number of memory instructions in the loop
 * n_o is the number of non-memory instructions in the loop
 * CPI: Cycles per instruction of the unprefetched loop
 *
 * USAGE: We use this formula slightly differently for each datatype
 * 
 * Contiguous: 
 * We always prefetch from L2 because we rely on the hardware prefetcher to
 * prefetch into L2 for us. This will have to be changed if the hardware 
 * prefetcher isn't effective (or doesn't exist)
 *
 * Vector:
 * < TODO>
 *
 * Indexed: 
 * We determine into which level the pack/unpack buffer will fit and use that 
 * in the numerator of the equation i.e. if the buffer fits entirely in L3, 
 * we set L_n = DLJIT_LATENCY_L3 and so on for smaller sizes. 
 * <TODO>: What if the indexed type doesn't fit into L3?
 *
 * The CPI values are also different for the different datatypes and are 
 * determined from experiments. We should find a better way of getting that 
 * value.
 *
 */

const long K = 100;

// For blockindexed types 
// This returns n, where n is the number of blocks to look ahead
uint64_t DLJIT_prefetch_distance(uint64_t blksize, uint64_t extent) {
    // TODO: Take blksize into account when calculating prefetch distance
    return 16;
    if(extent < DLJIT_CACHE_SIZE_L1) {
        return 0;
    } else if(extent < DLJIT_CACHE_SIZE_L2) {
        return 64;
    } else if(extent < DLJIT_CACHE_SIZE_L3) {
        return 32;
    }
    return 0;
}

// For vector types 
// This returns the number of bytes ahead to prefetch
uint64_t DLJIT_prefetch_distance(uint64_t blksize, uint64_t extent, int64_t stride) {
    if(stride < DLJIT_CACHE_LINE_SIZE)
        return 0;

    // TODO: Need to take blksize into account when calculating prefetch dist.
    if(stride < DLJIT_CACHE_SIZE_L1) {
        return 0;
    } else if(stride < DLJIT_CACHE_SIZE_L2) {
        return stride*4;
    } else if(stride < DLJIT_CACHE_SIZE_L3) {
        return stride*2;
    } else {
        return stride;
    }
}


// For contiguous buffers
// This returns the number of bytes ahead to prefetch
uint64_t DLJIT_prefetch_distance(uint64_t blksize) {
    return 4;
    if(blksize < DLJIT_CACHE_SIZE_L1)
        return 0;
    return DLJIT_CACHE_LINE_SIZE*6;
}
