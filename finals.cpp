/* This contains the functions to generate the *final types */

#include "jit.h"
#include "debug.h"

#include <llvm/IR/IntrinsicInst.h>
#include <sstream>
#include <iostream>

using namespace llvm;

static void create_prefetch(BuildEnv *env, long pd_seq, Value *seqptr, long pd_str, Value *strptr, Value *offsetsPtr, Value *idx, long blksize, int seqrw, int cachelevel, std::string dbgname) {
    IRBuilder<> *builder = env->builder();
    Module *module = env->module();

    auto prefFn = cast<Function>(Intrinsic::getDeclaration(module, Intrinsic::prefetch));

    for(long i = 0; i < blksize; i += DLJIT_CACHE_LINE_SIZE) {
        if(pd_seq) {
            Value *seqidx = builder->CreateAdd(env->int64Const(pd_seq),
                                               env->int64Const(i));
            Value *currseq = builder->CreateGEP(seqptr, seqidx);
            builder->CreateCall4(prefFn,
                                 currseq,
                                 env->int32Const(seqrw),
                                 env->int32Const(cachelevel),
                                 env->int32Const(1));
        }

        if(pd_str) {
            Value *offidx = builder->CreateAdd(idx, env->int64Const(pd_str));
            Value *offsetGEP = builder->CreateGEP(offsetsPtr, offidx);
            Value *offset = builder->CreateAdd(builder->CreateLoad(offsetGEP),
                                               env->int64Const(i));
            Value *currstr = builder->CreateGEP(strptr, offset);
            builder->CreateCall4(prefFn,
                                 currstr,
                                 env->int32Const((seqrw ? 0 : 1)),
                                 env->int32Const(cachelevel),
                                 env->int32Const(1));
        }
    }
}

static Function* DLJIT_gen_vecfinal(BuildEnv *env, Function *fn, Value *count, long blklen, long stride, long oldsize, long extent, unsigned flags, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();

    // fn->addFnAttr(Attribute::AttrKind::AlwaysInline);

    Value *src = getFnArg(fn, 1);
    Value *dst = getFnArg(fn, 0);

    int aligned = flag_aligned(flags);

    Function *fnA = DLJIT_gen_veccpy(env, blklen, stride, oldsize, extent, pack, true);
    Function *fnU = DLJIT_gen_veccpy(env, blklen, stride, oldsize, extent, pack, false);

    auto bbExit = BasicBlock::Create(*context, "exit", fn);
    auto bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);

    auto bbCheckAlign = BasicBlock::Create(*context, "align_check", fn,bbExit);
    auto bbAligned = BasicBlock::Create(*context, "aligned", fn, bbExit);
    auto bbUnaligned = BasicBlock::Create(*context, "unaligned", fn, bbExit);

    /* Entry */
    builder->SetInsertPoint(bbEntry);
    builder->CreateBr(bbCheckAlign);

    if(aligned) {
        /* Check for alignment */
        builder->SetInsertPoint(bbCheckAlign);
        Value *dstAsInt = builder->CreatePtrToInt(dst, env->int64Ty());
        Value *masked = builder->CreateAnd(dstAsInt, env->int64Const(31));
        Value *isaligned = builder->CreateICmpEQ(masked, env->int64Const(0));
        builder->CreateCondBr(isaligned, bbAligned, bbUnaligned);
    } else {
        builder->SetInsertPoint(bbCheckAlign);
        builder->CreateBr(bbUnaligned);
    }

    /* Aligned */
    Value *rvAligned = nullptr;
    builder->SetInsertPoint(bbAligned);
    rvAligned = builder->CreateCall3(fnA, dst, src, count);
    builder->CreateBr(bbExit);
    
    /* Unaligned */
    Value *rvUnaligned = nullptr;
    builder->SetInsertPoint(bbUnaligned);
    rvUnaligned = builder->CreateCall3(fnU, dst, src, count);
    builder->CreateBr(bbExit);

    /* Exit */
    builder->SetInsertPoint(bbExit);
    PHINode *rvPhi = builder->CreatePHI(env->int8PtrTy(), 2);
    rvPhi->addIncoming(rvAligned, bbAligned);
    rvPhi->addIncoming(rvUnaligned, bbUnaligned);
    builder->CreateRet(rvPhi);

    return fn;
}

Function* DLJIT_gen_vecfinal(BuildEnv *env, long count, long blklen, long stride, long oldsize, long extent, unsigned flags, bool pack) {
    Module *module = env->module();

    std::stringstream ss;
    ss << "vecfinal_" << (pack ? "pack" : "unpack") << "_all_" << blklen << "_" << stride << "_" << oldsize;
    std::string fname = ss.str();

    Function *fn = module->getFunction(fname);
    if(fn)
        return fn;

    std::vector<Type*> param_ts
        = { env->int8PtrTy(), /* src */
            env->int8PtrTy(), /* dst */ };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), param_ts, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));

    Value *llvmCount = env->int64Const(count);
    return DLJIT_gen_vecfinal(env, fn, llvmCount, blklen, stride, oldsize, extent, flags, pack);
}

Function* DLJIT_gen_vecfinal(BuildEnv *env, long blklen, long stride, long oldsize, long extent, unsigned flags, bool pack) {
    Module *module = env->module();

    std::stringstream ss;
    ss << "vecfinal_" << (pack ? "pack" : "unpack") << "_some_" << blklen << "_" << stride << "_" << oldsize;
    std::string fname = ss.str();

    Function *fn = module->getFunction(fname);
    if(fn)
        return fn;

    std::vector<Type*> param_ts
        = { env->int8PtrTy(), /* src */
            env->int8PtrTy(), /* dst */
            env->int64Ty()    /* count */ };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), param_ts, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));

    Value *count = getFnArg(fn, 2);
    return DLJIT_gen_vecfinal(env, fn, count, blklen, stride, oldsize, extent, flags, pack);
}

/* 
 * BLOCKINDEXED 
 */

static Function* DLJIT_gen_blockindexedfinal_aligned(BuildEnv *env, Function *fn, Value *begin, Value *end, long blklen, long offsets, long oldsize, long extent, unsigned flags, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module();

    // fn->addFnAttr(Attribute::AttrKind::AlwaysInline);

    long blksize = blklen*oldsize;

    Value *src = getFnArg(fn, 1);
    Value *dst = getFnArg(fn, 0);
    Value *llvmOffsets = env->int64Const(offsets);

    auto bbExit = BasicBlock::Create(*context, "exit", fn);
    auto bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);

    auto bbCheckAlign = BasicBlock::Create(*context, "check_align", fn, bbExit);
    auto bbABody = BasicBlock::Create(*context, "a_body", fn, bbExit);
    auto bbATail = BasicBlock::Create(*context, "a_tail", fn, bbExit);
    auto bbUBody = BasicBlock::Create(*context, "u_body", fn, bbExit);
    auto bbUTail = BasicBlock::Create(*context, "u_tail", fn, bbExit);

    uint64_t prefetch_seq = DLJIT_prefetch_distance(blksize);
    uint64_t prefetch_str = DLJIT_prefetch_distance(blksize, extent);
    
    
    DLJIT_gen_memcpy_n(env, blklen, oldsize, 0);
    auto a_memcpy = module->getFunction(getMemcpyName(blklen, oldsize, true));
    auto u_memcpy = module->getFunction(getMemcpyName(blklen, oldsize, false));

    /* Entry */
    builder->SetInsertPoint(bbEntry);
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());
    builder->CreateBr(bbCheckAlign);

    builder->SetInsertPoint(bbCheckAlign);
    Value *dstAsInt = builder->CreatePtrToInt(dst, env->int64Ty());
    Value *masked = builder->CreateAnd(dstAsInt, env->int64Const(31));
    Value *isaligned = builder->CreateICmpEQ(masked, env->int64Const(0));
    builder->CreateCondBr(isaligned, bbABody, bbUBody);

    Value *arv = nullptr, *urv = nullptr;
    {
    // Aligned Body
    builder->SetInsertPoint(bbABody);
    PHINode *phi = builder->CreatePHI(env->int64Ty(), 2);
    PHINode *ptrPhi = builder->CreatePHI(env->int8PtrTy(), 2);
    phi->addIncoming(begin, bbCheckAlign);
    if(pack)
        ptrPhi->addIncoming(dst, bbCheckAlign);
    else
        ptrPhi->addIncoming(src, bbCheckAlign);

    Value *offsetsGEP = builder->CreateGEP(offsetsPtr, phi);
    Value *offset = builder->CreateLoad(offsetsGEP);
    Value *srcPtr = nullptr, *dstPtr = nullptr;
    if(pack) {
        srcPtr = builder->CreateGEP(src, offset);
        dstPtr = ptrPhi;
        create_prefetch(env, prefetch_seq, dstPtr, prefetch_str, src, offsetsPtr, phi, blksize, 1, 3, "pack_prefdist");
    } else {
        srcPtr = ptrPhi;
        dstPtr = builder->CreateGEP(dst, offset);
        create_prefetch(env, prefetch_seq, srcPtr, prefetch_str, dst, offsetsPtr, phi, blksize, 0, 3, "unpack_prefdist");
    }
    
    builder->CreateCall2(a_memcpy, dstPtr, srcPtr);
    builder->CreateBr(bbATail);
        
    // Aligned Tail
    builder->SetInsertPoint(bbATail);
    Value *incr = builder->CreateAdd(phi, env->int64Const(1));
    phi->addIncoming(incr, bbATail);
    if(pack)
        arv = builder->CreateConstGEP1_64(dstPtr, blksize);
    else 
        arv = builder->CreateConstGEP1_64(srcPtr, blksize);
    ptrPhi->addIncoming(arv, bbATail);
    builder->CreateCondBr(builder->CreateICmpSLT(incr, end), bbABody, bbExit);
    }

    {
    // Unaligned Body
    builder->SetInsertPoint(bbUBody);
    PHINode *phi = builder->CreatePHI(env->int64Ty(), 2, "count");
    PHINode *ptrPhi = builder->CreatePHI(env->int8PtrTy(), 2, pack ? "d" : "s");
    phi->addIncoming(begin, bbCheckAlign);
    if(pack)
        ptrPhi->addIncoming(dst, bbCheckAlign);
    else
        ptrPhi->addIncoming(src, bbCheckAlign);

    Value *offsetsGEP = builder->CreateGEP(offsetsPtr, phi);
    Value *offset = builder->CreateLoad(offsetsGEP);
    Value *srcPtr = nullptr, *dstPtr = nullptr;
    if(pack) {
        srcPtr = builder->CreateGEP(src, offset);
        dstPtr = ptrPhi;
        create_prefetch(env, prefetch_seq, dstPtr, prefetch_str, src, offsetsPtr, phi, blksize, 1, 3, "pack_prefdist");
    } else {
        srcPtr = ptrPhi;
        dstPtr = builder->CreateGEP(dst, offset);
        create_prefetch(env, prefetch_seq, srcPtr, prefetch_str, dst, offsetsPtr, phi, blksize, 0, 3, "unpack_prefdist");
    }
    
    builder->CreateCall2(u_memcpy, dstPtr, srcPtr);
    builder->CreateBr(bbUTail);
        
    // Unaligned Tail
    builder->SetInsertPoint(bbUTail);
    Value *incr = builder->CreateAdd(phi, env->int64Const(1));
    phi->addIncoming(incr, bbUTail);
    if(pack)
        urv = builder->CreateConstGEP1_64(dstPtr, blksize);
    else 
        urv = builder->CreateConstGEP1_64(srcPtr, blksize);
    ptrPhi->addIncoming(urv, bbUTail);
    builder->CreateCondBr(builder->CreateICmpSLT(incr, end), bbUBody, bbExit);
    }

    /* Exit */
    builder->SetInsertPoint(bbExit);
    PHINode *rv = builder->CreatePHI(env->int8PtrTy(), 2);
    rv->addIncoming(arv, bbATail);
    rv->addIncoming(urv, bbUTail);
    builder->CreateRet(rv);

    return fn;
}

static Function* DLJIT_gen_blockindexedfinal_unaligned(BuildEnv *env, Function *fn, Value *begin, Value *end, long blklen, long offsets, long oldsize, long extent, unsigned flags, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module();

    // fn->addFnAttr(Attribute::AttrKind::AlwaysInline);
    long blksize = blklen*oldsize;

    Value *src = getFnArg(fn, 1);
    Value *dst = getFnArg(fn, 0);
    Value *llvmOffsets = env->int64Const(offsets);

    auto bbExit = BasicBlock::Create(*context, "exit", fn);
    auto bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);
    auto bbUBody = BasicBlock::Create(*context, "u_body", fn, bbExit);
    auto bbUTail = BasicBlock::Create(*context, "u_tail", fn, bbExit);

    uint64_t prefetch_seq = DLJIT_prefetch_distance(blksize);
    uint64_t prefetch_str = DLJIT_prefetch_distance(blksize, extent);
    
    DLJIT_gen_memcpy_n(env, blklen, oldsize, 0);
    auto u_memcpy = module->getFunction(getMemcpyName(blklen, oldsize, false));

    /* Entry */
    builder->SetInsertPoint(bbEntry);
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());
    builder->CreateBr(bbUBody);

    // Unaligned Body
    builder->SetInsertPoint(bbUBody);
    PHINode *phi = builder->CreatePHI(env->int64Ty(), 2, "count");
    PHINode *ptrPhi = builder->CreatePHI(env->int8PtrTy(), 2, pack ? "d" : "s");
    phi->addIncoming(begin, bbEntry);
    if(pack)
        ptrPhi->addIncoming(dst, bbEntry);
    else
        ptrPhi->addIncoming(src, bbEntry);

    Value *offsetsGEP = builder->CreateGEP(offsetsPtr, phi);
    Value *offset = builder->CreateLoad(offsetsGEP);
    Value *srcPtr = nullptr, *dstPtr = nullptr;
    if(pack) {
        srcPtr = builder->CreateGEP(src, offset);
        dstPtr = ptrPhi;
        create_prefetch(env, prefetch_seq, dstPtr, prefetch_str, src, offsetsPtr, phi, blksize, 1, 3, "pack_prefdist");
    } else {
        srcPtr = ptrPhi;
        dstPtr = builder->CreateGEP(dst, offset);
        create_prefetch(env, prefetch_seq, srcPtr, prefetch_str, dst, offsetsPtr, phi, blksize, 0, 3, "unpack_prefdist");
    }
    
    builder->CreateCall2(u_memcpy, dstPtr, srcPtr);
    builder->CreateBr(bbUTail);
        
    // Unaligned Tail
    Value *urv = nullptr;
    builder->SetInsertPoint(bbUTail);
    Value *incr = builder->CreateAdd(phi, env->int64Const(1));
    phi->addIncoming(incr, bbUTail);
    if(pack)
        urv = builder->CreateConstGEP1_64(dstPtr, blksize);
    else 
        urv = builder->CreateConstGEP1_64(srcPtr, blksize);
    ptrPhi->addIncoming(urv, bbUTail);
    builder->CreateCondBr(builder->CreateICmpSLT(incr, end), bbUBody, bbExit);

    /* Exit */
    builder->SetInsertPoint(bbExit);
    builder->CreateRet(urv);

    return fn;
}

static Function* DLJIT_gen_blockindexedfinal(BuildEnv *env, Function *fn, Value *begin, Value *end, long blklen, long offsets, long oldsize, long extent, unsigned flags, bool pack) {
    bool aligned = flag_aligned(flags);
    if(aligned) {
        return DLJIT_gen_blockindexedfinal_aligned(env, fn, begin, end, blklen, offsets, oldsize, extent, flags, pack);
    } else { 
        return DLJIT_gen_blockindexedfinal_unaligned(env, fn, begin, end, blklen, offsets, oldsize, extent, flags, pack);
    }
}

Function* DLJIT_gen_blockindexedfinal(BuildEnv *env, long count, long blklen, void* offsets, long oldsize, long extent, unsigned flags, bool pack) {
    Module *module = env->module();

    std::stringstream ss;
    ss << "blockindexedfinal_" << (pack ? "pack" : "unpack") << "_all_"
       << blklen << "_" << offsets << "_" << oldsize;
    std::string fname = ss.str();

    Function *fn = module->getFunction(fname);
    if(fn) return fn;

    std::vector<Type*> param_ts
        = { env->int8PtrTy(), /* src */
            env->int8PtrTy()  /* dst */ };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), param_ts, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));

    Value *begin = env->int64Const(0);
    Value *end = env->int64Const(count);
    return DLJIT_gen_blockindexedfinal(env, fn, begin, end, blklen, (long)offsets, oldsize, extent, flags, pack);
}

Function* DLJIT_gen_blockindexedfinal(BuildEnv *env, long blklen, void *offsets, long oldsize, long extent, unsigned flags, bool pack) {
    Module *module = env->module();
    
    std::stringstream ss;
    ss << "blockindexedfinal_" << (pack ? "pack" : "unpack") << "_some_"
       << blklen << "_" << offsets << "_" << oldsize;
    std::string fname = ss.str();

    Function *fn = module->getFunction(fname);
    if(fn) return fn;

    std::vector<Type*> param_ts
        = { env->int8PtrTy(), /* src */
            env->int8PtrTy(), /* dst */
            env->int64Ty(),   /* begin */
            env->int64Ty()    /* end */ };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), param_ts, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));

    Value *begin = getFnArg(fn,2);
    Value *end = getFnArg(fn,3);
    return DLJIT_gen_blockindexedfinal(env, fn, begin, end, blklen, (long)offsets, oldsize, extent, flags, pack);
}


/*
 * INDEXED
 */

static Function* DLJIT_gen_indexedfinal(BuildEnv *env, Function *fn, Value *begin, Value *end, long blklens, long offsets, long oldsize, long extent, unsigned flags, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Module *module = env->module();

    // fn->addFnAttr(Attribute::AttrKind::AlwaysInline);

    Value *src = getFnArg(fn, 1);
    Value *dst = getFnArg(fn, 0);

    Value *llvmBlklens = env->int64Const(blklens);
    Value *llvmOffsets = env->int64Const(offsets);
    Value *llvmOldsize = env->int64Const(oldsize);

    auto bbExit = BasicBlock::Create(*context, "exit", fn);
    auto bbEntry = BasicBlock::Create(*context, "entry", fn, bbExit);

    auto bbCheckAlign = BasicBlock::Create(*context, "align_check", fn, bbExit);
    auto bbBody = BasicBlock::Create(*context, "body", fn, bbExit);
    auto bbCopyLong = BasicBlock::Create(*context, "cpy_long", fn, bbExit);
    auto bbCopyShort = BasicBlock::Create(*context, "cpy_short", fn, bbExit);
    auto bbTail = BasicBlock::Create(*context, "tail", fn, bbExit);

	std::vector<Type*> tys 
        = { env->int8PtrTy(), env->int8PtrTy(), env->int64Ty() };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), tys, false);
	// Value *memcpyFn = Intrinsic::getDeclaration(module, Intrinsic::memcpy, tys);
    auto memcpyFn=cast<Function>(module->getOrInsertFunction("memcpy",fnType));
    // Function *memcpyFn = DLJIT_gen_memcpy(env);

    /* Entry */
    builder->SetInsertPoint(bbEntry);
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());
    builder->CreateBr(bbCheckAlign);

    /* TODO: We could conceivably do a runtime alignment check here
     * but that's for later */
    builder->SetInsertPoint(bbCheckAlign);
    builder->CreateBr(bbBody);

    /* Body */
    builder->SetInsertPoint(bbBody);
    PHINode *phi = builder->CreatePHI(env->int64Ty(), 2);
    PHINode *ptrPhi = builder->CreatePHI(env->int8PtrTy(), 2);
    phi->addIncoming(begin, bbCheckAlign);
    if(pack)
        ptrPhi->addIncoming(dst, bbCheckAlign);
    else
        ptrPhi->addIncoming(src, bbCheckAlign);
    Value *offsetsGEP = builder->CreateGEP(offsetsPtr, phi);
    Value *offset = builder->CreateLoad(offsetsGEP);
    Value *srcPtr = nullptr, *dstPtr = nullptr;
    if(pack) {
        srcPtr = builder->CreateGEP(src, offset);
        dstPtr = ptrPhi;
    } else {
        srcPtr = ptrPhi;
        dstPtr = builder->CreateGEP(dst, offset);
    }
    Value *blklensPtr = builder->CreateIntToPtr(llvmBlklens, env->int64PtrTy());
    Value *blklen = builder->CreateLoad(builder->CreateGEP(blklensPtr, phi));
    Value *blksize = builder->CreateMul(blklen, llvmOldsize);

    // builder->CreateCall3(memcpyFn, dstPtr, srcPtr, blksize);
    // builder->CreateBr(bbTail);
    builder->CreateCondBr(builder->CreateICmpUGT(blksize, env->int64Const(DLJIT_THRESHOLD_USE_MEMCPY)),
                          bbCopyLong, bbCopyShort);
    
    builder->SetInsertPoint(bbCopyLong);
    // builder->CreateCall5(memcpyFn, dstPtr, srcPtr, blksize, env->int32Const(0), env->int1Const(false));
    builder->CreateCall3(memcpyFn, srcPtr, dstPtr, blksize);
    builder->CreateBr(bbTail);

    // bbCopyShort will have been populated when gen_memcpy_short() is called 
    DLJIT_gen_memcpy_short(env, dstPtr, srcPtr, blklen, oldsize, fn, bbCopyShort, bbTail);

    /* Tail */
    builder->SetInsertPoint(bbTail);
    Value *incr = builder->CreateAdd(phi, env->int64Const(1));
    phi->addIncoming(incr, bbTail);
    Value *newPtr = builder->CreateGEP(ptrPhi, blksize);
    ptrPhi->addIncoming(newPtr, bbTail);
    builder->CreateCondBr(builder->CreateICmpSLT(incr, end), bbBody, bbExit);

    /* Exit */
    builder->SetInsertPoint(bbExit);
    builder->CreateRet(newPtr);

    return fn;
}

Function* DLJIT_gen_indexedfinal(BuildEnv *env, long count, void *blklens, void* offsets, long oldsize, long extent, unsigned flags, bool pack) {
    Module *module = env->module();

    std::stringstream ss;
    ss << "indexedfinal_" << (pack ? "pack" : "unpack") << "_all_"
       << blklens << "_" << offsets << "_" << oldsize;
    std::string fname = ss.str();

    Function *fn = module->getFunction(fname);
    if(fn) return fn;

    std::vector<Type*> param_ts
        = { env->int8PtrTy(), /* src */
            env->int8PtrTy()  /* dst */ };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), param_ts, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));

    Value *begin = env->int64Const(0);
    Value *end = env->int64Const(count);
    return DLJIT_gen_indexedfinal(env, fn, begin, end, (long)blklens, (long)offsets, oldsize, extent, flags, pack);
}

Function* DLJIT_gen_indexedfinal(BuildEnv *env, void *blklens, void *offsets, long oldsize, long extent, unsigned flags, bool pack) {
    Module *module = env->module();
    
    std::stringstream ss;
    ss << "indexedfinal_" << (pack ? "pack" : "unpack") << "_some_"
       << blklens << "_" << offsets << "_" << oldsize;
    std::string fname = ss.str();

    Function *fn = module->getFunction(fname);
    if(fn) return fn;

    std::vector<Type*> param_ts
        = { env->int8PtrTy(), /* src */
            env->int8PtrTy(), /* dst */
            env->int64Ty(),   /* begin */
            env->int64Ty()    /* end */ };
    FunctionType *fnType = FunctionType::get(env->int8PtrTy(), param_ts, false);
    fn = cast<Function>(module->getOrInsertFunction(fname, fnType));

    Value *begin = getFnArg(fn,2);
    Value *end = getFnArg(fn,3);
    return DLJIT_gen_indexedfinal(env, fn, begin, end, (long)blklens, (long)offsets, oldsize, extent, flags, pack);
}
