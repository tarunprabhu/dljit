#include "dljit.h"
#include "jit.h"

#include "llvm/MC/SubtargetFeature.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/Target/TargetOptions.h"

#include <sstream>

using namespace llvm;

extern TargetLibraryInfo *TLI;
extern TargetMachine *TM;

static TargetOptions GetTargetOptions() {
    TargetOptions Options;
  /* TODO: FIXME: We may need some target options here */
  return Options;
}

// Returns the TargetMachine instance or zero if no triple is provided.
static TargetMachine* GetTargetMachine(Triple TheTriple) {
    std::string MArch(MACHINE_ARCH);
    std::string MCPU(MACHINE_CPU);
    std::string Error;
    const Target *TheTarget = TargetRegistry::lookupTarget(MArch, Error);
    if (!TheTarget) {
        return 0;
    }

    // Package up features to be passed to target/subtarget
    std::string FeaturesStr;
    std::vector<std::string> MAttrs;
    std::istringstream ss;
    ss.str(MACHINE_ATTRS);
    for(std::string s; std::getline(ss, s, ' '); )
        MAttrs.push_back(s);
    if (MAttrs.size()) {
        SubtargetFeatures Features;
        for (unsigned i = 0; i != MAttrs.size(); ++i)
            Features.AddFeature(MAttrs[i]);
        FeaturesStr = Features.getString();
    }

    return TheTarget->createTargetMachine(TheTriple.getTriple(),
                                          MCPU, 
                                          FeaturesStr, 
                                          GetTargetOptions(),
                                          Reloc::Default, 
                                          CodeModel::Default,
                                          CodeGenOpt::Aggressive);
}


EXTERN_C void DLJIT_initialize() {
    LLVM_NATIVE_TARGETINFO();
    LLVM_NATIVE_TARGET();
    LLVM_NATIVE_TARGETMC();
    LLVM_NATIVE_ASMPRINTER();

    TLI = nullptr;
    TM = nullptr;

    // Initialize passes
    PassRegistry &Registry = *PassRegistry::getPassRegistry();
    initializeCore(Registry);
    initializeScalarOpts(Registry);
    initializeObjCARCOpts(Registry);
    initializeVectorization(Registry);
    initializeIPO(Registry);
    initializeAnalysis(Registry);
    initializeIPA(Registry);
    initializeTransformUtils(Registry);
    initializeInstCombine(Registry);
    initializeInstrumentation(Registry);
    initializeTarget(Registry);
    // For codegen passes, only passes that do IR to IR transformation are
    // supported.
    initializeCodeGenPreparePass(Registry);

    // Add an appropriate TargetLibraryInfo pass for the module's triple.
    Triple triple(MACHINE_TRIPLE);
    TLI = new TargetLibraryInfo(Triple(triple));

    if(triple.getArch()) {
        TM = GetTargetMachine(Triple(triple));
    }
}

EXTERN_C void DLJIT_finalize() {
    // I don't think there's anything that needs cleanup here
}

EXTERN_C void* DLJIT_compile(void* ptr) {
    const Dataloop *dl = static_cast<const Dataloop*>(ptr);
    return DLJIT_compile_all(dl);
}

EXTERN_C DLJIT_function_ptr DLJIT_function_pack(void* ptr) {
    DLJIT_wrapper *wrapper = static_cast<DLJIT_wrapper*>(ptr);
    return wrapper->getPack();
}

EXTERN_C DLJIT_function_ptr DLJIT_function_unpack(void* ptr) {
    DLJIT_wrapper *wrapper = static_cast<DLJIT_wrapper*>(ptr);
    return wrapper->getUnpack();
}

EXTERN_C void DLJIT_free(void* ptr) {
    DLJIT_wrapper *wrapper = static_cast<DLJIT_wrapper*>(ptr);
    delete wrapper;
}
