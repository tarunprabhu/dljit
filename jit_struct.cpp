#include "jit.h"
#include <sstream>

using namespace llvm;

static BasicBlock *DLJIT_struct_push(BuildEnv *env, int sp, BasicBlock *bb, int lookupKey, bool pack) {
    IRBuilder<> *builder = env->builder();
    const Dataloop *dl = env->dl();
    
    long count = dl[sp].count;
    long offset0 = dl[sp].s.s_t.offsets[0];
    long blklen0 = dl[sp].s.s_t.blklens[0];

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(count);
    Constant *llvmBlklen0 = env->int64Const(blklen0);

    builder->SetInsertPoint(bb);
    Value *base = nullptr;
    if(pack) 
        base = builder->CreateLoad(env->inbase());
    else 
        base = builder->CreateLoad(env->outbase());

    {
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    builder->CreateStore(base, baseGEP);      // stack[sp].base = base
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmCount, leftGEP); // stack[sp].left = dl[sp].count
    }    

    {
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    builder->CreateStore(base, baseGEP);         // stack[sp].base = base
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmBlklen0, leftGEP); // stack[sp+1].left = blklen0
    Value *newBase = builder->CreateConstGEP1_64(base, offset0);
    if(pack) 
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    }
    builder->CreateBr(env->SLTLookup(lookupKey, 0));

    return bb;
}

static BasicBlock *DLJIT_struct_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, int lookupKey, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();
    std::stringstream ss;

    long extent = dl[sp].extent;
    long count = dl[sp].count;
    Value *stack = env->stack();

    Value *llvmCount = env->int64Const(count);
    Value *llvmOffsets = env->int64Const((long)dl[sp].s.s_t.offsets);
    Value *llvmBlklens = env->int64Const((long)dl[sp].s.s_t.blklens);

    ss.str(std::string());
    ss << "struct" << sp << "_done";
    BasicBlock *bbDone = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    ss.str(std::string());
    ss << "struct" << sp << "_left";
    BasicBlock *bbLeft = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    /* Entry */
    builder->SetInsertPoint(bb);
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());
    Value *blklensPtr = builder->CreateIntToPtr(llvmBlklens, env->int64PtrTy());

    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *currBase = builder->CreateLoad(baseGEP);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    Value *currLeft = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(currLeft, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbLeft);

    /* Done */
    builder->SetInsertPoint(bbDone);
    {
    Value *newBase = builder->CreateConstGEP1_64(currBase, extent);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    builder->CreateBr(bbOuter);
    }

    /* Left */
    builder->SetInsertPoint(bbLeft);
    {
    Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *left1GEP = builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
    
    Value *idx = builder->CreateSub(llvmCount, newLeft);
    Value *offset = builder->CreateLoad(builder->CreateGEP(offsetsPtr, idx));
    Value *newBase = builder->CreateGEP(currBase, offset);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());

    Value *blklen = builder->CreateLoad(builder->CreateGEP(blklensPtr, idx));
    builder->CreateStore(blklen, left1GEP);

    Value *lookupTable = env->SLTGetStackVar(lookupKey);
    std::vector<Value*> indices = { env->int64Const(0), idx };
    Value *jmpGEP = builder->CreateInBoundsGEP(lookupTable, indices);
    Value *jmpBlock = builder->CreateLoad(jmpGEP);
    std::vector<BasicBlock*> jmpDsts = env->SLTGetBlocks(lookupKey);
    IndirectBrInst *br = builder->CreateIndirectBr(jmpBlock, jmpDsts.size());
    for(unsigned j = 1; j < jmpDsts.size(); j++)
        br->addDestination(jmpDsts[j]);
    }
    return bb;
}

BasicBlock* DLJIT_struct(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();
    std::stringstream ss;

    long count = dl[sp].count;

    int key = env->SLTCreate(count);

    ss.str(std::string());
    ss << "struct" << sp << "_pop";
    BasicBlock *bbPop = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    env->setDataloop(dl[sp].s.s_t.dls[0]);
    BasicBlock *bbFirstInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbFirstInner) 
        return nullptr;
    env->SLTAdd(key, bbFirstInner);
    for(int i = 1; i < count; i++) {
        env->setDataloop(dl[sp].s.s_t.dls[i]);
        BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
        if(!bbInner) return nullptr;
        env->SLTAdd(key, bbInner);
    }
    env->setDataloop(dl);

    ss.str(std::string());
    ss << "struct" << sp << "_push";
    BasicBlock *bbPush=BasicBlock::Create(*context, ss.str(), fn, bbFirstInner);

    auto rvPop = DLJIT_struct_pop(env, sp, bbPop, bbOuter, key, bbIns, pack);
    auto rvPush = DLJIT_struct_push(env, sp, bbPush, key, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}
