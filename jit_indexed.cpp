#include "jit.h"
#include "debug.h"

#include <sstream>

using namespace llvm;

static BasicBlock* DLJIT_idx_push(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbInner, bool pack) {
    IRBuilder<> *builder = env->builder();
    const Dataloop *dl = env->dl();

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmBlklen0 = env->int64Const(dl[sp].s.i_t.blklens[0]);

    builder->SetInsertPoint(bb);
    Value *base = nullptr;
    if(pack)
        base = builder->CreateLoad(env->inbase());
    else 
        base = builder->CreateLoad(env->outbase());

    {
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    builder->CreateStore(base, baseGEP);      // stack[sp].base = base
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmCount, leftGEP); // stack[sp].left = dl[sp].count
    }

    {
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    builder->CreateStore(base, baseGEP);        // stack[sp+1].base = base
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmBlklen0, leftGEP); // stack[sp+1].base = blklens[0]
    Value *newBase = builder->CreateConstGEP1_64(base, dl[sp].s.i_t.offsets[0]);
    if(pack) 
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    }

    // goto dlpush
    builder->CreateBr(bbInner);

    return bb;
}

static BasicBlock* DLJIT_idx_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();
    std::stringstream ss;

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmOffsets = env->int64Const((long)dl[sp].s.i_t.offsets);
    Constant *llvmBlklens = env->int64Const((long)dl[sp].s.i_t.blklens);

    ss.str(std::string());
    ss << "idx" << sp << "_done";
    BasicBlock *bbDone = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    ss.str(std::string());
    ss << "idx" << sp << "_left";
    BasicBlock *bbLeft = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    // if(stack[sp].countLeft == 0)
    builder->SetInsertPoint(bb);
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());
    Value *blklensPtr = builder->CreateIntToPtr(llvmBlklens, env->int64PtrTy());
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *currBase = builder->CreateLoad(baseGEP);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    Value *currLeft = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(currLeft, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbLeft);

    // goto dlpop
    builder->SetInsertPoint(bbDone);
    builder->CreateBr(bbOuter);

    // Not done
    builder->SetInsertPoint(bbLeft);
    {
    // idx = count - countLeft
    Value *idx = builder->CreateSub(llvmCount, newLeft);
    // (in/out)base = stack.base + dl.offsets[idx]
    Value *offset = builder->CreateLoad(builder->CreateGEP(offsetsPtr, idx));
    Value *newBase = builder->CreateGEP(currBase, offset);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    // stack[sp+1].countLeft = dl.blklens[idx]
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    Value *blklen = builder->CreateLoad(builder->CreateGEP(blklensPtr, idx));
    builder->CreateStore(blklen, leftGEP);
    builder->CreateBr(bbInner);
    }

    return bb;
}

BasicBlock* DLJIT_indexed(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    std::stringstream ss;

    ss.str(std::string());
    ss << "idx" << sp << "_pop";
    BasicBlock *bbPop = BasicBlock::Create(*context, ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) return nullptr;

    ss.str(std::string());
    ss << "idx" << sp << "_push";
    BasicBlock *bbPush = BasicBlock::Create(*context, ss.str(), fn, bbInner);

    auto rvPop = DLJIT_idx_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_idx_push(env, sp, bbPush, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

static BasicBlock* DLJIT_idxfinal_push(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();
    
    long size = dl[sp].size;
    long count = dl[sp].count;
    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    void* blklens = (void*)dl[sp].s.i_t.blklens;
    void* offsets = (void*)dl[sp].s.i_t.offsets;
    long oldsize = dl[sp].s.i_t.oldsize;

    Value *stack = env->stack();
    Constant *llvmSize = env->int64Const(size);
    Constant *llvmCount = env->int64Const(count);
    Constant *llvmOffsets = env->int64Const((long)offsets);
    Constant *llvmBlklens = env->int64Const((long)blklens);
    Constant *llvmOldsize = env->int64Const(oldsize);

    Function *fnPackAll = DLJIT_gen_indexedfinal(env, count, blklens, offsets, oldsize, extent, flags, pack);
    Function *fnPackSome = DLJIT_gen_indexedfinal(env, blklens, offsets, oldsize, extent, flags, pack);

    auto bbAll = BasicBlock::Create(*context, "if_push_all", fn, bbIns);
    auto bbCountBody = BasicBlock::Create(*context, "if_push_count_body", fn, bbIns);
    auto bbCountTail = BasicBlock::Create(*context, "if_push_count_tail", fn, bbIns);
    auto bbCount = BasicBlock::Create(*context, "if_push_count", fn, bbIns);
    auto bbSome = BasicBlock::Create(*context, "if_push_some", fn, bbIns);
    auto bbNone = BasicBlock::Create(*context, "if_push_none", fn, bbIns);

    /* Entry */
    builder->SetInsertPoint(bb);
    Value *inbase = builder->CreateLoad(env->inbase());
    Value *outbase = builder->CreateLoad(env->outbase());
    Value *sizeleft = builder->CreateLoad(env->sizeLeft());
    Value *blklensPtr = builder->CreateIntToPtr(llvmBlklens, env->int64PtrTy());
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());

    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    if(pack)
        builder->CreateStore(inbase, baseGEP);
    else 
        builder->CreateStore(outbase, baseGEP);
    Value *currBase = builder->CreateLoad(baseGEP);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    builder->CreateStore(llvmCount, leftGEP);
    Value *currLeft = builder->CreateLoad(leftGEP);
    builder->CreateCondBr(builder->CreateICmpSGE(sizeleft, llvmSize),
                          bbAll, bbCountBody);

    /* All */
    builder->SetInsertPoint(bbAll);
    {
    Value *newInbase = nullptr, *newOutbase = nullptr;
    if(pack) {
        newOutbase = builder->CreateCall2(fnPackAll, outbase, currBase);
        newInbase = builder->CreateConstGEP1_64(currBase, extent);
    } else {
        newInbase = builder->CreateCall2(fnPackAll, currBase, inbase);
        newOutbase = builder->CreateConstGEP1_64(currBase, extent);
    }
    builder->CreateStore(newOutbase, env->outbase());
    builder->CreateStore(newInbase, env->inbase());
    builder->CreateStore(env->int64Const(0), leftGEP);
    builder->CreateStore(builder->CreateSub(sizeleft,llvmSize),env->sizeLeft());
    builder->CreateBr(bbOuter);
    }

    /* Count body */
    builder->SetInsertPoint(bbCountBody);
    PHINode *countPhi = builder->CreatePHI(env->int64Ty(), 2);
    PHINode *copiedPhi = builder->CreatePHI(env->int64Ty(), 2);
    countPhi->addIncoming(env->int64Const(0), bb);
    copiedPhi->addIncoming(env->int64Const(0), bb);
    builder->CreateBr(bbCountTail);

    /* Count tail */
    builder->SetInsertPoint(bbCountTail);
    {
    Value *blklen=builder->CreateLoad(builder->CreateGEP(blklensPtr, countPhi));
    Value *blksize = builder->CreateMul(llvmOldsize, blklen);
    Value *newCopied = builder->CreateAdd(copiedPhi, blksize);
    Value *newCount = builder->CreateAdd(countPhi, env->int64Const(1));
    countPhi->addIncoming(newCount, bbCountTail);
    copiedPhi->addIncoming(newCopied, bbCountTail);
    Value *countDone = builder->CreateICmpSLT(newCount, llvmCount);
    Value *copiedDone = builder->CreateICmpSLE(newCopied, sizeleft);
    builder->CreateCondBr(builder->CreateAnd(countDone, copiedDone),
                          bbCountBody, bbCount);
    }

    /* Count */
    builder->SetInsertPoint(bbCount);
    builder->CreateCondBr(builder->CreateICmpSGT(countPhi, env->int64Const(0)),
                          bbSome, bbNone);

    /* Some */
    builder->SetInsertPoint(bbSome);
    {
    if(pack) {
        Value *newPtr = builder->CreateCall4(fnPackSome, outbase, currBase, env->int64Const(0), countPhi);
        builder->CreateStore(newPtr, env->outbase());
    } else { 
        Value *newPtr = builder->CreateCall4(fnPackSome, currBase, inbase, env->int64Const(0), countPhi);
        builder->CreateStore(newPtr, env->inbase());
    }
    // sizeLeft -= copied
    Value *newSizeLeft = builder->CreateSub(sizeleft, copiedPhi);
    builder->CreateStore(newSizeLeft, env->sizeLeft());
    // stack[sp].countLeft -= count
    Value *newLeft = builder->CreateSub(currLeft, countPhi);
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateBr(bbNone);
    }

    /* None */
    builder->SetInsertPoint(bbNone);
    {
    Value *idx = countPhi;
    Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *left1GEP = builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
    Value *blklen = builder->CreateLoad(builder->CreateGEP(blklensPtr, idx));
    builder->CreateStore(blklen, left1GEP);
    Value *offset = builder->CreateLoad(builder->CreateGEP(offsetsPtr, idx));
    Value *newBase = builder->CreateGEP(currBase, offset);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    // goto dlpush
    builder->CreateBr(bbInner);

    }

    return bb;
}

static BasicBlock* DLJIT_idxfinal_pop(BuildEnv *env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    IRBuilder<> *builder = env->builder();
    LLVMContext *context = env->context();
    Function *fn = env->fn();
    const Dataloop *dl = env->dl();

    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    void *blklens = (void*)dl[sp].s.i_t.blklens;
    void *offsets = (void*)dl[sp].s.i_t.offsets;
    long oldsize = dl[sp].s.i_t.oldsize;

    Value *stack = env->stack();
    Constant *llvmCount = env->int64Const(dl[sp].count);
    Constant *llvmOldsize = env->int64Const(dl[sp].s.i_t.oldsize);
    Constant *llvmOffsets = env->int64Const((long)dl[sp].s.i_t.offsets);
    Constant *llvmBlklens = env->int64Const((long)dl[sp].s.i_t.blklens);
    
    Function *fnPackSome = DLJIT_gen_indexedfinal(env, blklens, offsets, oldsize, extent, flags, pack);

    auto bbCountHead = BasicBlock::Create(*context, "if_pop_count_head", fn, bbIns);
    auto bbCountBody = BasicBlock::Create(*context, "if_pop_count_body", fn, bbIns);
    auto bbCountTail = BasicBlock::Create(*context, "if_pop_count_tail", fn, bbIns);
    auto bbCount = BasicBlock::Create(*context, "if_pop_count", fn, bbIns);
    auto bbSome = BasicBlock::Create(*context, "if_pop_some", fn, bbIns);
    auto bbNone = BasicBlock::Create(*context, "if_pop_none", fn, bbIns);
    auto bbDone = BasicBlock::Create(*context, "if_pop_done", fn, bbIns);

    /* Entry */
    builder->SetInsertPoint(bb);
    Value *inbase = builder->CreateLoad(env->inbase());
    Value *outbase = builder->CreateLoad(env->outbase());
    Value *sizeleft = builder->CreateLoad(env->sizeLeft());
    Value *blklensPtr = builder->CreateIntToPtr(llvmBlklens, env->int64PtrTy());
    Value *offsetsPtr = builder->CreateIntToPtr(llvmOffsets, env->int64PtrTy());
    
    Value *selm = builder->CreateConstGEP2_64(stack, 0, sp);
    Value *baseGEP = builder->CreateStructGEP(selm, STACK_BASE_IDX);
    Value *currBase = builder->CreateLoad(baseGEP);
    Value *leftGEP = builder->CreateStructGEP(selm, STACK_LEFT_IDX);
    Value *currLeft = builder->CreateLoad(leftGEP);
    Value *newLeft = builder->CreateSub(currLeft, env->int64Const(1));
    builder->CreateStore(newLeft, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(newLeft, env->int64Const(0)),
                          bbDone, bbCountHead);

    /* Count head */
    builder->SetInsertPoint(bbCountHead);
    Value *idx = builder->CreateSub(llvmCount, newLeft);
    builder->CreateBr(bbCountBody);

    /* Count body */
    builder->SetInsertPoint(bbCountBody);
    PHINode *countPhi = builder->CreatePHI(env->int64Ty(), 2);
    PHINode *copiedPhi = builder->CreatePHI(env->int64Ty(), 2);
    countPhi->addIncoming(idx, bbCountHead);
    copiedPhi->addIncoming(env->int64Const(0), bbCountHead);
    builder->CreateBr(bbCountTail);

    /* Count tail */
    builder->SetInsertPoint(bbCountTail);
    {
    Value *blklen=builder->CreateLoad(builder->CreateGEP(blklensPtr, countPhi));
    Value *blksize = builder->CreateMul(llvmOldsize, blklen);
    Value *newCopied = builder->CreateAdd(copiedPhi, blksize);
    Value *newCount = builder->CreateAdd(countPhi, env->int64Const(1));
    countPhi->addIncoming(newCount, bbCountTail);
    copiedPhi->addIncoming(newCopied, bbCountTail);
    Value *countDone = builder->CreateICmpSLT(newCount, llvmCount);
    Value *copiedDone = builder->CreateICmpSLE(newCopied, sizeleft);
    builder->CreateCondBr(builder->CreateAnd(countDone, copiedDone),
                          bbCountBody, bbCount);
    }

    /* Count */
    builder->SetInsertPoint(bbCount);
    builder->CreateCondBr(builder->CreateICmpSGT(countPhi, idx),
                          bbSome, bbNone);

    /* Some */
    builder->SetInsertPoint(bbSome);
    {
    if(pack) {
        Value *newPtr = builder->CreateCall4(fnPackSome, outbase, currBase, idx, countPhi);
        builder->CreateStore(newPtr, env->outbase());
    } else { 
        Value *newPtr = builder->CreateCall4(fnPackSome, currBase, inbase, idx, countPhi);
        builder->CreateStore(newPtr, env->inbase());
    }
    Value *newSizeLeft = builder->CreateSub(sizeleft, copiedPhi);
    builder->CreateStore(newSizeLeft, env->sizeLeft());
    Value *nl = builder->CreateSub(newLeft, builder->CreateSub(countPhi,idx));
    builder->CreateStore(nl, leftGEP);
    builder->CreateCondBr(builder->CreateICmpEQ(nl, env->int64Const(0)),
                          bbDone, bbNone);
    }

    /* None */
    builder->SetInsertPoint(bbNone);
    {
    Value *idx = countPhi;
    Value *selm1 = builder->CreateConstGEP2_64(stack, 0, sp+1);
    Value *left1GEP = builder->CreateStructGEP(selm1, STACK_LEFT_IDX);
    Value *blklen = builder->CreateLoad(builder->CreateGEP(blklensPtr, idx));
    builder->CreateStore(blklen, left1GEP);
    Value *offset = builder->CreateLoad(builder->CreateGEP(offsetsPtr, idx));
    Value *newBase = builder->CreateGEP(currBase, offset);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    builder->CreateBr(bbInner);
    }

    /* Done */
    builder->SetInsertPoint(bbDone);
    {
    Value *newBase = builder->CreateConstGEP1_64(currBase, extent);
    if(pack)
        builder->CreateStore(newBase, env->inbase());
    else 
        builder->CreateStore(newBase, env->outbase());
    builder->CreateBr(bbOuter);
    }

    return bb;
}

BasicBlock* DLJIT_indexedfinal(BuildEnv* env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    LLVMContext *context = env->context();
    Function *fn = env->fn();

    BasicBlock *bbPop = BasicBlock::Create(*context, "if_pop", fn, bbIns);
    BasicBlock *bbInner = DLJIT_contigfinal(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner)
        return nullptr;
    BasicBlock *bbPush = BasicBlock::Create(*context,"if_push", fn, bbInner);
    auto rvPop = DLJIT_idxfinal_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_idxfinal_push(env, sp, bbPush, bbOuter, bbInner, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

