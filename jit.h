#ifndef TLP_DATATYPES_DLJIT_H
#define TLP_DATATYPES_DLJIT_H

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Value.h"

#include "mpi.h"

#include <vector>
#include <map>

#include "dataloop.h"
#include "machineinfo.h"
#include "wrapper.h"

/* Parameter indices for pack and unpack functions */
#define PARAM_IN 0
#define PARAM_OUT 1
#define PARAM_SIZE 2
#define PARAM_STATE 3
#define PARAM_COPIED 4

#define STATE_SP_IDX 0
#define STATE_DL_IDX 1
#define STATE_PARTIAL_IDX 2
#define STATE_STACK_IDX 3

#define STACK_BASE_IDX 0
#define STACK_PREV_IDX 1
#define STACK_LEFT_IDX 2

/* This is here to encapsulate all the constants and other information that 
 * we need to generate the code. There's too much to pass around as function
 * arguments and I really don't like global variables */
class BuildEnv {
private:
    llvm::Module *_module;
    const Dataloop *_dl;
    llvm::LLVMContext *_ctxt;
    llvm::IRBuilder<> *_builder;
    /* Current function. Could be either dl_pack or dl_unpack */
    llvm::Function *_currfn;

    /* These are the addresses of all the contigfinal blocks in the dataloop
       (including those inside struct types) into which the (un)pack may 
       resume */
    std::vector<llvm::BasicBlock*> _resumeBlocks;

    std::map<unsigned, std::vector<llvm::BasicBlock*>> _slt;
    std::vector<llvm::AllocaInst*> _sltStackVars;

    /* User-defined types */
    llvm::StructType *_selmType;
    llvm::StructType *_stateType;
  
    /* Local variables */
    llvm::Value *_stack;
    llvm::AllocaInst *_sizeLeft;
    llvm::AllocaInst *_inbase;
    llvm::AllocaInst *_outbase;
    llvm::AllocaInst *_partial;

public:
    BuildEnv(llvm::Module *module, llvm::LLVMContext *context, const Dataloop *dl);
    ~BuildEnv();

    /* This is used to initialize the stack variables, counters and bases.
     * The constructor only initializes the types, context and builder */
    void initStackVars(llvm::Function *fn, llvm::BasicBlock *bbEntry);

    llvm::Module *module()         const { return _module; }
    const Dataloop* dl()           const { return _dl; }
    llvm::IRBuilder<>* builder()   const { return _builder; }
    llvm::LLVMContext* context()   const { return _ctxt; }
    llvm::Function* fn()           const { return _currfn; }

    void setDataloop(const Dataloop *dl) { _dl = dl; }

    std::vector<llvm::BasicBlock*> resumeBlocks() const { return _resumeBlocks;}
    unsigned addResumeBlock(llvm::BasicBlock *bb) { 
        _resumeBlocks.push_back(bb); 
        return _resumeBlocks.size()-1;
    }
    unsigned numResumeBlocks() const { return _resumeBlocks.size(); }

    llvm::Type* voidTy()         const { return llvm::Type::getVoidTy(*_ctxt); }

    llvm::IntegerType* int1Ty()  const { return llvm::Type::getInt1Ty(*_ctxt); }
    llvm::IntegerType* int8Ty()  const { return llvm::Type::getInt8Ty(*_ctxt); }
    llvm::IntegerType* int16Ty() const { return llvm::Type::getInt16Ty(*_ctxt);}
    llvm::IntegerType* int32Ty() const { return llvm::Type::getInt32Ty(*_ctxt);}
    llvm::IntegerType* int64Ty() const { return llvm::Type::getInt64Ty(*_ctxt);}
    llvm::VectorType*  vecTy(llvm::Type *inner, uint64_t count) const { 
        return llvm::VectorType::get(inner, count);
    }

    llvm::PointerType* int8PtrTy()  const { return llvm::Type::getInt8PtrTy(*_ctxt); }
    llvm::PointerType* int16PtrTy() const { return llvm::Type::getInt16PtrTy(*_ctxt); }
    llvm::PointerType* int32PtrTy() const { return llvm::Type::getInt32PtrTy(*_ctxt); }
    llvm::PointerType* int64PtrTy() const { return llvm::Type::getInt64PtrTy(*_ctxt); }

    llvm::PointerType* int8PtrPtrTy() const { return llvm::PointerType::get(int8PtrTy(), 0); }
    llvm::PointerType* vecPtrTy(llvm::Type *inner, uint64_t count) const {
        return vecTy(inner, count)->getPointerTo();
    }

    llvm::ConstantInt* int1Const(bool b) const { 
        return llvm::ConstantInt::get(int1Ty(), b, 0); 
    }
    llvm::ConstantInt* int32Const(int n, int sign=0) const {
        return llvm::ConstantInt::get(int32Ty(), n, sign);
    }
    llvm::ConstantInt* int64Const(long n, int sign=0) const { 
        return llvm::ConstantInt::get(int64Ty(), n, sign); 
    }

    llvm::StructType* stateTy()     const { return _stateType; }
    llvm::PointerType* statePtrTy() const { return llvm::PointerType::get(_stateType, 0); }

    llvm::AllocaInst* inbase()      const { return _inbase; }
    llvm::AllocaInst* outbase()     const { return _outbase; }
    llvm::AllocaInst* sizeLeft()    const { return _sizeLeft; }
    llvm::AllocaInst* partial()     const { return _partial; } 
    
    llvm::Value* stack()            const { return _stack; }
    llvm::Value* setStack(llvm::Value *stack) { _stack = stack; return stack; }

    unsigned SLTCreate(long count);
    std::vector<llvm::BasicBlock*> SLTGetBlocks(unsigned key) const;
    llvm::BasicBlock* SLTLookup(unsigned key, unsigned idx) const;
    bool SLTAdd(unsigned key, llvm::BasicBlock *bb);
    unsigned SLTGetNumStackVars() const { return _sltStackVars.size(); }
    llvm::AllocaInst* SLTGetStackVar(unsigned key) const;
};

/* Helper functions */
llvm::Argument* getFnArg(llvm::Function *fn, unsigned idx);
std::string getMemcpyName(long blklen, long oldsize, bool aligned);

/* Optimization functions */
uint64_t DLJIT_prefetch_distance(uint64_t blksize, uint64_t extent, int64_t stride); // Vector
uint64_t DLJIT_prefetch_distance(uint64_t blksize, uint64_t extent); // Indexed
uint64_t DLJIT_prefetch_distance(uint64_t blksize); // Contig

/* Routines called from wrapper file */
DLJIT_wrapper* DLJIT_compile_all(const Dataloop *dl);

/* Code generation functions */
llvm::BasicBlock* DLJIT_gen_loop(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);

llvm::BasicBlock* DLJIT_contiguous(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_vector(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_blockindexed(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_indexed(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_contigfinal(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_vectorfinal(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_blockindexedfinal(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_indexedfinal(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_struct(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_contigchild(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);
llvm::BasicBlock* DLJIT_returnto(BuildEnv*, int, llvm::BasicBlock*, llvm::BasicBlock*, bool);

llvm::Function* DLJIT_gen_save_state(BuildEnv *env);
llvm::BasicBlock* DLJIT_gen_memcpy_short(BuildEnv *env, llvm::Value *dst, llvm::Value *src, llvm::Value *count, long basesize, llvm::Function *fn, llvm::BasicBlock *bbEntry, llvm::BasicBlock *bbExit);
llvm::Function* DLJIT_gen_memcpy_n(BuildEnv *env, long blklen, long oldsize, long extent);
llvm::Function* DLJIT_gen_veccpy(BuildEnv*, long, long, long, long, bool, bool);


llvm::Function* DLJIT_gen_vecfinal(BuildEnv*, long, long, long, long, unsigned, bool); // All
llvm::Function* DLJIT_gen_vecfinal(BuildEnv*, long, long, long, long, long, unsigned, bool); //Some
llvm::Function* DLJIT_gen_blockindexedfinal(BuildEnv*, long, void*, long, long, unsigned, bool); // All 
llvm::Function* DLJIT_gen_blockindexedfinal(BuildEnv*, long, long, void*, long, long, unsigned, bool); // Some
llvm::Function* DLJIT_gen_indexedfinal(BuildEnv*, long, void*, void*, long, long, unsigned, bool); // All 
llvm::Function* DLJIT_gen_indexedfinal(BuildEnv*, void*, void*, long, long, unsigned, bool); // Some


#endif
